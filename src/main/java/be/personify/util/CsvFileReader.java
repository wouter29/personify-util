package be.personify.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 * 
 * @author vanderw
 *
 */
public class CsvFileReader {
	
	
	/**
	 * Reads csv file with specified header
	 * @param reader that reads
	 * @param header containing the header info
	 * @return a list containing the records
	 * @throws Exception with the stacktrace
	 */
	public static List<CSVRecord> readCsvFile(Reader reader, String[] header) throws Exception {
		CSVParser csvFileParser = null;
		try {
			csvFileParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader(header).withCommentMarker('#'));
			return csvFileParser.getRecords();
		}
		catch (Exception e) {
			throw e;
		} 
		finally {
			try {
				if ( reader != null ) {
					reader.close();
				}
				if ( csvFileParser != null ) {
					csvFileParser.close();
				}
			} catch (IOException e) {

			}
		}
	}
	
	
	/**
	 * Reads the csvfile
	 * @param reader for reading
	 * @param csvFormat the format used to read
	 * @return the list with the records
	 * @throws Exception with the stacktrace
	 */
	public static List<CSVRecord> readCsvFile(Reader reader, CSVFormat csvFormat) throws Exception {
		CSVParser csvFileParser = null;
		try {
			csvFileParser = new CSVParser(reader, csvFormat);
			return csvFileParser.getRecords();
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			try {
				if ( reader != null ) {
					reader.close();
				}
				if ( csvFileParser != null ) {
					csvFileParser.close();
				}
			} catch (IOException e) {

			}
		}
	}
	
	
	
	
	/**
	 * Reads a csv file with the given header definition
	 * @param file containing the content
	 * @param header containing header information
	 * @return a list containing the records
	 * @throws Exception with the stacktrace
	 */
	public static List<CSVRecord> readCsvFile(File file, String[] header) throws Exception {

		FileReader fileReader = new FileReader(file);
		return readCsvFile(fileReader, header);
	}
	
	
	public static List<CSVRecord> readCsvFile( String filePath ) throws Exception {
		InputStream inputStream = CsvFileReader.class.getClassLoader().getResourceAsStream(filePath);
		return readCsvFile(new InputStreamReader(inputStream), CSVFormat.DEFAULT);
		
	}
	
	
	
}
