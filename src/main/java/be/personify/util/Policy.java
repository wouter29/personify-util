package be.personify.util;

import java.io.Serializable;
import java.util.List;

public class Policy implements Serializable{
	
	private static final long serialVersionUID = -2230200389392418855L;

	private Permission permission;

	private Decision decision;
	
	private List<PolicyCondition> policyConditions;



	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Decision getDecision() {
		return decision;
	}

	public void setDecision(Decision decision) {
		this.decision = decision;
	}

	public List<PolicyCondition> getPolicyConditions() {
		return policyConditions;
	}

	public void setPolicyConditions(List<PolicyCondition> policyConditions) {
		this.policyConditions = policyConditions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((decision == null) ? 0 : decision.hashCode());
		result = prime * result + ((permission == null) ? 0 : permission.hashCode());
		result = prime * result + ((policyConditions == null) ? 0 : policyConditions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Policy other = (Policy) obj;
		if (decision != other.decision)
			return false;
		if (permission == null) {
			if (other.permission != null)
				return false;
		} else if (!permission.equals(other.permission))
			return false;
		if (policyConditions == null) {
			if (other.policyConditions != null)
				return false;
		} else if (!policyConditions.equals(other.policyConditions))
			return false;
		return true;
	}
	
	
	
	

}
