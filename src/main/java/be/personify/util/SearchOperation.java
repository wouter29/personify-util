package be.personify.util;

public enum SearchOperation {

	EQUALS("eq"),
	NOT_EQUALS("ne"),
	CONTAINS("co"),
	STARTS_WITH("sw"),
	ENDS_WITH("ew"),
	PRESENT("pr", 2),
	GREATER_THEN("gt"),
	GREATER_THEN_OR_EQUAL("ge"),
	LESS_THEN("lt"),
	LESS_THEN_EQUAL("le");
	
	
	
	private String code;
	private int parts;
	
	private SearchOperation ( String code) {
		this.code = code;
		parts = 3;
	}
	
	private SearchOperation ( String code, int parts ) {
		this.code = code;
		this.parts = parts;
	}
	
	
	public static SearchOperation operationFromString(String s) {
		for( SearchOperation o : SearchOperation.values() ) {
			if ( o.code.equalsIgnoreCase(s)) {
				return o;
			}
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getParts() {
		return parts;
	}

	public void setParts(int parts) {
		this.parts = parts;
	}
	
	
	
}
