package be.personify.util;

public enum State {
	
	PRESENT,
	ABSENT,
	DISABLED,
	ARCHIVED,
	ERROR

}
