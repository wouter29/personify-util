package be.personify.util.audit;

public class InvalidConfigurationException extends Exception {
	
	
	private static final long serialVersionUID = 4251408511785909122L;

	public InvalidConfigurationException() {
		super();
	}

	
	public InvalidConfigurationException(String message) {
		super(message);
	}
	
	public InvalidConfigurationException(String message, Throwable t) {
		super(message,t);
	}
	
}
