package be.personify.util.audit;

/**
 * Enum defining the possible logger implementations
 */
public enum AuditLoggerImplementations {
	
	/**
	 * kafka implementation	
	 */
	kafka("be.personify.audit.kafka.AuditLoggerKafkaImpl");
	
	private String className;
	
	private AuditLoggerImplementations( String className ) {
		this.className = className;
	}
	

	/**
	 * Gets the classname of the logger
	 * @return String containing the classname
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Sets the classname of the logger
	 * @param className the classname
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	
	
	

}
