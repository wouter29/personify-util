package be.personify.util.audit;

import java.util.Map;

public interface AuditLogger {
	
		
	public void configure(Map<String,Object> configuration ) throws InvalidConfigurationException;
	
	public void log(String entity, String action, String id, Object object );
	
	public String name();
	
}
