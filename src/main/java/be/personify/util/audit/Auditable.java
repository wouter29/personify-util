package be.personify.util.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Auditable {
	
	boolean enabled() default true;
    String action() default "";
    String entity() default "";
    String[] loggers() default {};

}
