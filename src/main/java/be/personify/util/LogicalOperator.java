package be.personify.util;

public enum LogicalOperator {
	
	AND,
	OR,
	NOT

}
