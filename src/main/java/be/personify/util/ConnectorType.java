package be.personify.util;

public enum ConnectorType {
	
	LDAP("be.personify.iam.provisioning.connectors.LdapConnector"),
	REST("be.personify.iam.provisioning.connectors.RestConnector"),
	MSGRAPH_USER("be.personify.iam.provisioning.connectors.MicrosoftGraphUserConnector"),
	MSGRAPH_GROUP("be.personify.iam.provisioning.connectors.MicrosoftGraphGroupConnector"),
	DATABASE("be.personify.iam.provisioning.connectors.DatabaseConnector"),
	SCIM("be.personify.iam.provisioning.connectors.ScimConnector"),
	SSH("be.personify.iam.provisioning.connectors.SshConnector"),
	FILE("be.personify.iam.provisioning.connectors.FileConnector"),
	PSNFYIDENTITY("be.personify.iam.provisioning.connectors.PersonifyIdentityConnector"),
	PSNFYORGANISATION("be.personify.iam.provisioning.connectors.PersonifyOrganisationConnector"),
	PSNFYORGASS("be.personify.iam.provisioning.connectors.PersonifyOrganisationAssignmentConnector"),
	PSNFYENTASS("be.personify.iam.provisioning.connectors.PersonifyEntitlementAssignmentConnector"),
	CUSTOM("");
	

	private String className;
	
	private ConnectorType ( String className ) {
		this.className = className;
	}

	public String getClassName() {
		return className;
	}
	
	

}
