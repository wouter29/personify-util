package be.personify.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple sortcriteria
 * @author vanderw
 *
 */
public class SortCriteria implements Serializable {
	
	private static final long serialVersionUID = 6020373022517713585L;

	private List<SortCriterium> criteria = new ArrayList<>();

	/**
	 * constructor with array of criteriums
	 * @param criteriums the criteriums
	 */
	public SortCriteria( SortCriterium...criteriums ) {
		for ( SortCriterium sc : criteriums ) {
			criteria.add(sc);
		}
	}
	


	public List<SortCriterium> getCriteria() {
		return criteria;
	}


	public void setCriteria(List<SortCriterium> criteria) {
		this.criteria = criteria;
	}


	@Override
	public String toString() {
		return "SortCriteria [criteria=" + criteria + "]";
	}

	
	
	
	
	
}
