package be.personify.util.scim;

public enum ScimMutability {
	
	immutable,
	readWrite,
	readOnly

}
