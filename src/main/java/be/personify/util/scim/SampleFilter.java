package be.personify.util.scim;

public class SampleFilter {
	
	public static final String FILTER_ONE = "userName eq \"bjensen\"";
	public static final String FILTER_ONE_BIS = "userName eq \"bjensen six\"";
	public static final String FILTER_TWO = "title pr";
	public static final String FILTER_THREE = "title pr and userType eq \"Employee\"";
	public static final String FILTER_FOUR = "title pr or userType eq \"Intern\"";
	public static final String FILTER_FIVE = "userType eq \"Employee\" and (emails co \"example.com\" or emails.value co \"example.org\")";
	public static final String FILTER_FIVE_BIS = "(emails co \"example.com\" or emails.value co \"example.org\") and userType eq \"Employee\"";
	public static final String FILTER_FIVE_TRIS = "(emails co \"example.com\" or emails.value co \"example.org\") or userType eq \"Employee\"";
	public static final String FILTER_SIX = "userType eq \"Employee\" and (emails.type eq \"work\")";
	public static final String FILTER_SEVEN = "emails[type eq \"work\" and value co \"@example.com\"] or ims[type eq \"xmpp\" and value co \"@foo.com\"]";
	public static final String FILTER_EIGHT = "(id eq \"df7af4be-6851-423d-a41e-e32dc3e5a17e\") or (id eq \"01f91eaf-b2fe-4781-8c21-f0644b8db62d\") or (id eq \"7d8cbb88-4c75-4dd5-94e1-12b48632fbdc\")";
	public static final String FILTER_EIGHT_BIS = "((id eq \"df7af4be-6851-423d-a41e-e32dc3e5a17e\") or (id eq \"01f91eaf-b2fe-4781-8c21-f0644b8db62d\") or (id eq \"7d8cbb88-4c75-4dd5-94e1-12b48632fbdc\"))";
	public static final String FILTER_EIGHT_TRIS = "( (id eq \"df7af4be-6851-423d-a41e-e32dc3e5a17e\") or (id eq \"01f91eaf-b2fe-4781-8c21-f0644b8db62d\") or (id eq \"7d8cbb88-4c75-4dd5-94e1-12b48632fbdc\") )";


}
