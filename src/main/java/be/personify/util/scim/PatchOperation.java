package be.personify.util.scim;

public enum PatchOperation {
	
	add,
	remove,
	replace

}
