package be.personify.util.captcha;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import be.personify.util.http.HttpHeaders;
import be.personify.util.http.HttpMethod;
import be.personify.util.io.IOUtils;

public class Recaptcha {
	
	private static final Logger logger = LoggerFactory.getLogger(Recaptcha.class);
	
	private static final String APPLICATION_X_WWW_FORM_URLENCODED_CHARSET_UTF_8 = "application/x-www-form-urlencoded; charset=UTF-8";
	private static final String CAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify";
	private static final String SECRET = "secret=";
	private static final String RESPONSE2 = "&response=";
	private static final ObjectMapper objectMapper = new ObjectMapper();
	
	public static final String REQUEST_PARAMETER = "g-recaptcha-response";
	

	public static synchronized boolean isCaptchaValid(String secretKey, String response) {
		long start = System.currentTimeMillis();
	    try {
	        String url = CAPTCHA_URL; 
	        String params = SECRET + secretKey + RESPONSE2 + response;

	        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
	        http.setDoOutput(true);
	        http.setRequestMethod(HttpMethod.POST.name());
	        http.setRequestProperty(HttpHeaders.CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED_CHARSET_UTF_8);
	        OutputStream out = http.getOutputStream();
	        out.write(params.getBytes(IOUtils.CHARSET_UTF_8));
	        out.flush();
	        out.close();

	        InputStream res = http.getInputStream();
	        BufferedReader rd = new BufferedReader(new InputStreamReader(res, IOUtils.CHARSET_UTF_8));

	        
	        StringBuilder sb = new StringBuilder();
	        int cp;
	        while ((cp = rd.read()) != -1) {
	            sb.append((char) cp);
	        }
	        
	        Map<String,Object> map = (Map<String,Object>)objectMapper.readValue(sb.toString(), Map.class);
	        res.close();
	        
	        if ( map.containsKey("success")) {
	        	boolean b = (boolean)map.get("success");
	        	logger.info("recaptcha validated to [{}] in [{} ms]", b, (System.currentTimeMillis() - start));
	        	return b;
	        }
	        
	        return false;
	        
	    } catch (Exception e) {
	        logger.error("can not validate captcha ", e);
	    }
	    return false;
	}
	
	
	
	
}
