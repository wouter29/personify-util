package be.personify.util;

import java.io.Serializable;

public class KeyValue implements Serializable {
	
	private static final long serialVersionUID = 3149299455056424351L;

	public KeyValue( String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	private String key;
	
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
 
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	

}
