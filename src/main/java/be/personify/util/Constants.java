package be.personify.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Constants {
	
	private static final String DATE_OF_BIRTH_FORMAT = "dd/MM/yyyy";
	
	public static final String ID = "id";
	
	public static final ObjectMapper objectMapper = new ObjectMapper();
	
	public static DateFormat getDateOfBirthFormat() {
		return new SimpleDateFormat(DATE_OF_BIRTH_FORMAT);
	}

}
