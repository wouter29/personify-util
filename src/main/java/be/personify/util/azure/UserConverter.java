package be.personify.util.azure;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import be.personify.util.StringUtils;
import be.personify.util.http.HttpHeaders;
import be.personify.util.io.IOUtils;

public class UserConverter {
	
	
	private static final String[] types = new String[] {"BV","NV", "VZW", "SA", "LTD"};
	
	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	
	private static final String token = "eyJ0eXAiOiJKV1QiLCJub25jZSI6IjcxUGFOVGd0QjNwbG4ydmRGVnp4QklsaE9fODRPdGFjbGk5VU84b1dXSkEiLCJhbGciOiJSUzI1NiIsIng1dCI6IjVPZjlQNUY5Z0NDd0NtRjJCT0hIeEREUS1EayIsImtpZCI6IjVPZjlQNUY5Z0NDd0NtRjJCT0hIeEREUS1EayJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC80OWMzZDcwMy0zNTc5LTQ3YmYtYTg4OC03YzkxM2ZiZGNlZDkvIiwiaWF0IjoxNjA5MjM5MTU3LCJuYmYiOjE2MDkyMzkxNTcsImV4cCI6MTYwOTI0MzA1NywiYWNjdCI6MCwiYWNyIjoiMSIsImFjcnMiOlsidXJuOnVzZXI6cmVnaXN0ZXJzZWN1cml0eWluZm8iLCJ1cm46bWljcm9zb2Z0OnJlcTEiLCJ1cm46bWljcm9zb2Z0OnJlcTIiLCJ1cm46bWljcm9zb2Z0OnJlcTMiLCJjMSIsImMyIiwiYzMiLCJjNCIsImM1IiwiYzYiLCJjNyIsImM4IiwiYzkiLCJjMTAiLCJjMTEiLCJjMTIiLCJjMTMiLCJjMTQiLCJjMTUiLCJjMTYiLCJjMTciLCJjMTgiLCJjMTkiLCJjMjAiLCJjMjEiLCJjMjIiLCJjMjMiLCJjMjQiLCJjMjUiXSwiYWlvIjoiQVVRQXUvOFNBQUFBUmJ2cHNYeW90RzY0cWJwVzRBTWJXTW82NUkwUnQ2ak5rUGtVSk1tNTRFUSthODNrYUFzb0ZZbStGVHpKeWF5M3Ntb2g5TlNNQXBoOTZ1VCt5akM3emc9PSIsImFtciI6WyJwd2QiLCJtZmEiXSwiYXBwX2Rpc3BsYXluYW1lIjoiR3JhcGggZXhwbG9yZXIgKG9mZmljaWFsIHNpdGUpIiwiYXBwaWQiOiJkZThiYzhiNS1kOWY5LTQ4YjEtYThhZC1iNzQ4ZGE3MjUwNjQiLCJhcHBpZGFjciI6IjAiLCJmYW1pbHlfbmFtZSI6IlZhbiBEZXIgQmVrZW4iLCJnaXZlbl9uYW1lIjoiV291dGVyIiwiaWR0eXAiOiJ1c2VyIiwiaXBhZGRyIjoiODEuMTY0LjE0Ny4xNzUiLCJuYW1lIjoiV291dGVyIFZhbiBEZXIgQmVrZW4iLCJvaWQiOiJlZDEyZTM0Yi1mODdiLTQxZjYtYjdiOC1kZTA4YTc4OGY5MWIiLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtNjMzMjA2MTgtNTU5OTQ0MDU3LTE1OTM1OTE5OTYtNzI2MTAiLCJwbGF0ZiI6IjE0IiwicHVpZCI6IjEwMDMzRkZGQTMxRjgyQ0UiLCJyaCI6IjAuQUFBQUE5ZkRTWGsxdjBlb2lIeVJQNzNPMmJYSWk5NzUyYkZJcUsyM1NOcHlVR1FDQUg4LiIsInNjcCI6IkNhbGVuZGFycy5SZWFkV3JpdGUgQ29udGFjdHMuUmVhZFdyaXRlIERpcmVjdG9yeS5SZWFkLkFsbCBGaWxlcy5SZWFkV3JpdGUuQWxsIEdyb3VwLlJlYWQuQWxsIEdyb3VwLlJlYWRXcml0ZS5BbGwgTWFpbC5SZWFkV3JpdGUgTm90ZXMuUmVhZFdyaXRlLkFsbCBvcGVuaWQgT3JnQ29udGFjdC5SZWFkLkFsbCBQZW9wbGUuUmVhZCBwcm9maWxlIFNpdGVzLlJlYWRXcml0ZS5BbGwgVGFza3MuUmVhZFdyaXRlIFVzZXIuUmVhZCBVc2VyLlJlYWQuQWxsIFVzZXIuUmVhZEJhc2ljLkFsbCBVc2VyLlJlYWRXcml0ZSBlbWFpbCIsInNpZ25pbl9zdGF0ZSI6WyJrbXNpIl0sInN1YiI6IjRJcTZTVFJydGRyckNFc3hPeVJtUHFDY3Q4eXZja04ySmlsb1VMckxkOXMiLCJ0ZW5hbnRfcmVnaW9uX3Njb3BlIjoiRVUiLCJ0aWQiOiI0OWMzZDcwMy0zNTc5LTQ3YmYtYTg4OC03YzkxM2ZiZGNlZDkiLCJ1bmlxdWVfbmFtZSI6InZhbmRldzE5QGNyb25vcy5iZSIsInVwbiI6InZhbmRldzE5QGNyb25vcy5iZSIsInV0aSI6ImJ2ZEhiWFNpU2txa2dxcGNSU1BlQUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbImI3OWZiZjRkLTNlZjktNDY4OS04MTQzLTc2YjE5NGU4NTUwOSJdLCJ4bXNfc3QiOnsic3ViIjoiSkZGUHBnSThiLUxYMjlvNUVKLWFUeG9fdmJvLXhEeGZrTUtveHBNeVZmdyJ9LCJ4bXNfdGNkdCI6MTMwMzg0OTk3MH0.i-bDoXPZaYURWQGAOABrgfkKGh1xNnSqiT9--ha-pKMKwS7C-1qRP8FJM0_-7AW8PNnVI4tdlX02bypwgwe15dvwUQKvFONBx1iTh_SvmsqAyeNU2zRrVH0j8UEw2ZTsf7Tv_g8_bPOJL6VfxnVi4wW6DigQFDOhynzvWMA_KThp-FGL_PrqrQ1-awiUw3_oxGOGVZbjdxbk-ZVbKLhjWyIFYMh_l5QtQzdIXUZXMns9oSbOj-kaZMw8w4Pro6U6TXdfwgrA9xgxqnIxXIHibuKIbSCNKMWIgyrIzeZFEWBEPgSZxalmNUvmNlJUvCb_xVsrV8VOCIefahK__LDBbQ";
	
	public static void main(String[] args) {
		
		UserConverter converter = new UserConverter();
		File directory = new File("/home/wouter/Projects/mogo-generator/generator/src/main/resources/datasets/vault/dataset-identit/cronos_users");
		converter.convert( directory );
		
	}

	private void convert(File directory) {
		String[] files = directory.list();
		int total = 0;
		int orgCount = 0;
		Map<String,String> companies = new HashMap<>();
		List<String> companyNames = new ArrayList<>();
		StringBuffer userBuffer = new StringBuffer("");
		StringBuffer organisationsBuffer = new StringBuffer("");
		StringBuffer organisationAssignmentsBuffer = new StringBuffer("");
		Map<String,String> managerOrgMap = new HashMap<>();
		for ( String s : files ) {
			if ( s.startsWith("user_") ) {
				File f = new File(directory, s);
				System.out.println("file " + f.getAbsolutePath());
				try {
					Map m = MAPPER.readValue(f, Map.class);
					List<Map> users = (List)m.get("value");
					for ( Map user : users ) {
						String userType = (String)user.get("userType");
						if ( userType == null || !userType.equals("Guest") ) {
							Boolean accountEnabled = (Boolean)user.get("accountEnabled");
							if ( accountEnabled != null && !accountEnabled == false) {
								String upn = (String)user.get("userPrincipalName");
								//vandew123
								String before = upn.split("@")[0];
								if ( upn.endsWith("@cronos.be") && before.length() < 10) {
									
									String firstName = (String)user.get("givenName");
									userBuffer.append(firstName.replaceAll(",", StringUtils.EMPTY_STRING) + StringUtils.COMMA);
									userBuffer.append(user.get("surname") + StringUtils.COMMA);
									userBuffer.append(StringUtils.EMPTY_STRING + StringUtils.COMMA);
									userBuffer.append(upn + StringUtils.COMMA );
									String email = ((String)user.get("mail"));
									if ( email != null) {
										email = email.toLowerCase();
									}
									else {
										email = upn;
									}
									
									userBuffer.append( email + StringUtils.COMMA);
									String mobile = (String)user.get("mobilePhone");
									if ( StringUtils.isEmpty(mobile)) {
										userBuffer.append(StringUtils.EMPTY_STRING + StringUtils.COMMA);
									}
									else {
										userBuffer.append(user.get("mobilePhone") + StringUtils.COMMA);
									}
									
									userBuffer.append( "INTERNAL" + StringUtils.COMMA);
									
									userBuffer.append(IOUtils.LINE_SEPARATOR);
									
									String companyName = (String)user.get("companyName");
									if ( companyName != null ) {
										companyName = companyName.trim();
									}
									String organisationCode = null;
									if ( companyName != null && !companies.containsKey(companyName)) {
										orgCount++;
										organisationCode = generateOrganisationCode(companyName, orgCount);
										companies.put(companyName,organisationCode);
										String organisationType = extractOrganisationType(companyName);
										if ( organisationType != null ) {
											companyName = companyName.replaceAll(StringUtils.SPACE + organisationType, StringUtils.EMPTY_STRING);
											if ( companyNames.contains(companyName.toLowerCase())) {
												companyName = companyName + StringUtils.SPACE + organisationType;
											}
										}
										else {
											organisationType = "SME";
											if ( companyNames.contains(companyName.toLowerCase())) {
												companyName = companyName + StringUtils.SPACE + organisationType;
											}
										}
										companyNames.add(companyName.toLowerCase());
										
										organisationsBuffer.append(companyName).append(StringUtils.COMMA)
										.append(organisationCode).append(StringUtils.COMMA)
										.append(organisationType).append(StringUtils.COMMA).append("true,true").append(IOUtils.LINE_SEPARATOR);
									}
									else {
										organisationCode = companies.get(companyName);
									}
									
									if ( organisationCode != null ) {
										organisationAssignmentsBuffer.append(upn).append(StringUtils.COMMA).append(organisationCode).append(StringUtils.COMMA).append(IOUtils.LINE_SEPARATOR);
										getTheManager(managerOrgMap, user, organisationCode);
									}
									
									
									
									
									
									total ++;
								}
							}
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		System.out.println("user : " + userBuffer.toString());
		File identitiesFile = new File(directory,"IDENTITY.csv");
		System.out.println("total users " + total);
		System.out.println("total companies " + companies.size());
		
		FileWriter writer;
		try {
			writer = new FileWriter(identitiesFile);
			writer.write(userBuffer.toString());
			writer.flush();
			writer = new FileWriter(new File(directory,"ORGANISATION.csv"));
			writer.write(organisationsBuffer.toString());
			writer.flush();
			writer = new FileWriter(new File(directory,"ORGANISATIONASSIGNMENT.csv"));
			writer.write(organisationAssignmentsBuffer.toString());
			writer.flush();
			
			StringBuffer b = new StringBuffer("");
			for ( String key : managerOrgMap.keySet()) {
				String value = managerOrgMap.get(key);
				String[] organisations = value.split(StringUtils.COMMA);
				for ( String o : organisations) {
					b.append(key + StringUtils.COMMA + o + StringUtils.COMMA + "ADMIN_ORG").append(IOUtils.LINE_SEPARATOR);
				}
			}
			writer = new FileWriter(new File(directory,"ENTITLEMENTASSIGNMENT.csv"));
			writer.write(b.toString());
			writer.flush();
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	private void getTheManager(Map<String, String> managerOrgMap, Map user, String organisationCode) {
		//get the manager
		RestTemplate restTemplate = new RestTemplate();
		 HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory() {
				
				@Override
				public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
					ClientHttpRequest request = super.createRequest(uri, httpMethod);
					request.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
					return request;
				}
			    	
			};
			
			restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
		try {
			String id = (String)user.get("id");
			ResponseEntity<Map> responseEntity = restTemplate.exchange(new RequestEntity<>(HttpMethod.GET, new URI("https://graph.microsoft.com/v1.0/users/" + id + "/manager") ), Map.class);
			
			if ( responseEntity != null ) {
				String userPrincipal = (String)responseEntity.getBody().get("userPrincipalName");
				System.out.println("manegr found for org " + userPrincipal + "  " + organisationCode);
				if ( managerOrgMap.containsKey(userPrincipal)) {
					String list = managerOrgMap.get(userPrincipal);
					if (!list.contains(organisationCode)) {
						list = list + StringUtils.COMMA + organisationCode;
						managerOrgMap.put(userPrincipal, list);
					}
				}
				else {
					managerOrgMap.put(userPrincipal, organisationCode);
				}
			}
			
			
			
		} catch (RestClientException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	private String generateOrganisationCode(String companyName, int count) {
		System.out.println(companyName);
		if ( companyName.length() < 3 ) {
			companyName = companyName + "0";
		}
		String start = companyName.substring(0,3).toUpperCase();
		String end = "00000";
		String cc = "" + count;
		end = end.substring(0,end.length() - cc.length()) + cc;
		return start + end;
	}
	
	
	private String extractOrganisationType( String companyName ) {
		 for ( String type : types) {
			 if (companyName.contains(" " + type + " ")) {
				 return type;
			 }
			 else if (companyName.endsWith(" " + type)) {
				 return type;
			 }
		}
		return null;		
		
	}
	
}
