package be.personify.util;

import java.io.Serializable;

public class PolicyCondition implements Serializable {
	
	private static final long serialVersionUID = -3296941043141657104L;

	private String conditionExpression;


	public String getConditionExpression() {
		return conditionExpression;
	}


	public void setConditionExpression(String conditionExpression) {
		this.conditionExpression = conditionExpression;
	}
	
	
	
}
