package be.personify.util;

import java.io.Serializable;

public enum Decision implements Serializable {
	
	ALLOW,DENY;

}
