package be.personify.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	
	
	private static final String PATTERN_CAMEL_CASE = "(?<=[a-z])[A-Z]";
	private static final String S = "s";
	private static final String Y = "y";
	private static final String SS = "ss";
	private static final String ES = "es";
	private static final String IES = "ies";
	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";
	public static final String ZERO = "0";
	public static final String SLASH = "/";
	public static final String DASH = "-";
	public static final String PIPE = "|";
	public static final String COLON = ":";
	public static final String LEFT_PARENTHESE = "(";
	public static final String RIGHT_PARENTHESE = ")";
	public static final String LEFT_BRACKET = "[";
	public static final String RIGHT_BRACKET = "]";
	public static final String COMMA = ",";
	public static final String DOT = ".";
	public static final String SEMI_COLON = ";";
	public static final String QUESTION_MARK = "?";
	public static final String COLON_SLASHSLASH = "://";
	public static final String ESCAPED_PIPE = "\\|";
	public static final String AMPERSAND = "&";
	public static final String EQUALS = "=";
	public static final String NOT_EQUALS = "!=";
	public static final String GT = ">";
	public static final String LT = "<";
	public static final String UNDERSCORE = "_";
	public static final String STAR = "*";
	public static final String PERCENT = "%";
	public static final String SPACE_DASH_SPACE = " - ";
	public static final String QUOTE = "\"";
	public static final String SINGLE_QUOTE = "'";
	
	public static final String KEY = "key";
	
	
	public static final String CRLF = "\r\n";
	public static final String TWO_DASHES = "--";
	public static final String BOUNDARY =  "*****";
	
	public static final String MULTIPART_START = "multipart/";
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
		
		
		
	
	public static boolean isEmpty( String s ) {
		return s == null || s.length() == 0;
	}
	
	public static boolean isEmpty( List<Object> list ) {
		return list == null || list.size() == 0;
	}
	
	public static boolean isEmpty(Object str) {
		return (str == null || EMPTY_STRING.equals(str));
	}
	 
	
	public static String join(List<String> list, String delim) {

	    StringBuilder sb = new StringBuilder();

	    String loopDelim = EMPTY_STRING;

	    for(String s : list) {

	        sb.append(loopDelim);
	        sb.append(s);            

	        loopDelim = delim;
	    }

	    return sb.toString();
	}
	
	
	
	public static String capitalize( String s ) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
	
	
	
	
	public static String getMultiIdentifier( String single ) {
		if ( single.endsWith(Y)) {
			if ( !single.endsWith("Key")) {
				return single.substring(0, single.length()-1) + IES;
			}
		}
		if ( single.endsWith(SS)) {
			return single + ES;
		}
		return single + S;
	}
	
	
	public static String getSingleIdentifier( String multi ) {
		if ( multi.endsWith(IES)) {
			return multi.substring(0, multi.length() -3) + Y;
		}
		return multi.substring(0, multi.length() -1);
	}
	
	
	
	public static String toCamelCase(String value) {
		Matcher m = Pattern.compile(PATTERN_CAMEL_CASE).matcher(value);

		StringBuffer sb = new StringBuffer();
		while (m.find()) {
		    m.appendReplacement(sb, UNDERSCORE + m.group().toLowerCase());
		}
		m.appendTail(sb);
		
		return sb.toString();
	}
	
	

}
