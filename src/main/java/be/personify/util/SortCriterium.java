package be.personify.util;

import java.io.Serializable;

/**
 * A simple sort criterium class
 * @author vanderw
 *
 */
public class SortCriterium implements Serializable {

	private static final long serialVersionUID = -3184919196138992744L;

	private String attributeName;

	private SortOrder sortOrder;
	
	
	/**
	 * The constructor
	 * @param a
	 * @param s
	 */
	public SortCriterium(String a, SortOrder s) {
		this.attributeName = a;
		this.sortOrder = s;
	}


	public String getAttributeName() {
		return attributeName;
	}


	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}


	public SortOrder getSortOrder() {
		return sortOrder;
	}


	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}


	@Override
	public String toString() {
		return "SortCriterium [attributeName=" + attributeName + ", sortOrder=" + sortOrder + "]";
	}

	
	
	
	
	
}
