package be.personify.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple searchcriteria
 * @author vanderw
 *
 */
public class SearchCriteria implements Serializable {
	
	private static final long serialVersionUID = 6020373022517713585L;

	private List<SearchCriterium> criteria = new ArrayList<>();
	
	private List<SearchCriteria> groupedCriteria = new ArrayList<>();
	
	
	private LogicalOperator operator = LogicalOperator.AND;
	
	/**
	 * constructor with array of criteriums
	 * @param criteriums the array of criteriums
	 */
	public SearchCriteria( LogicalOperator operator, SearchCriterium...criteriums ) {
		this.operator = operator;
		for ( SearchCriterium sc : criteriums ) {
			criteria.add(sc);
		}
	}

	
	/**
	 * constructor with array of criteriums
	 * @param criteriums the array of criteriums
	 */
	public SearchCriteria( SearchCriterium...criteriums ) {
		for ( SearchCriterium sc : criteriums ) {
			criteria.add(sc);
		}
	}
	
	/**
	 * Gets the criteria
	 * @return the list containing the criteria
	 */
	public List<SearchCriterium> getCriteria() {
		return criteria;
	}

	/**
	 * Sets the criteria
	 * @param criteria the criteria to be set
	 */
	public void setCriteria(List<SearchCriterium> criteria) {
		this.criteria = criteria;
	}
	
	
	
	

	public List<SearchCriteria> getGroupedCriteria() {
		return groupedCriteria;
	}


	public void setGroupedCriteria(List<SearchCriteria> groupedCriteria) {
		this.groupedCriteria = groupedCriteria;
	}


	public LogicalOperator getOperator() {
		return operator;
	}


	public void setOperator(LogicalOperator operator) {
		this.operator = operator;
	}


	@Override
	public String toString() {
		return "SearchCriteria [criteria=" + criteria + "] GroupedCriteria " + groupedCriteria + "] [logicalOperator=" + operator + "]";
	}
	
	public int size() {
		return ( criteria.size() + groupedCriteria.size());
	}
	
	
	public boolean containsKey( String key ) {
		for ( SearchCriterium c : getCriteria()) {
			if ( c.getKey().equalsIgnoreCase(key) || c.getKey().toLowerCase().startsWith(key.toLowerCase() + StringUtils.DOT)) {
				return true;
			}
		}
		for ( SearchCriteria c : getGroupedCriteria()) {
			if ( c.containsKey(key)) {
				return true;
			}
		}
		return false;
	}

	
	
	
	
	
}
