package be.personify.util.http;

/**
 * enum listing the possible http protocols
 * @author vanderw
 *
 */
public enum HttpProtocol {
	
	HTTP,
	HTTPS;

}
 