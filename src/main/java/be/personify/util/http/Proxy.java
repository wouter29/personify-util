package be.personify.util.http;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import be.personify.util.StringUtils;
import be.personify.util.io.IOUtils;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

/**
 * Class to proxy requests
 * 
 * @author vanderw
 *
 */
public class Proxy {

	private static final String IDENTITY = "identity";

	private static final Logger logger = LoggerFactory.getLogger(Proxy.class);

	// host
	private static final String[] inclusions = new String[] { "referer", "origin" };

	private static final String[] exclusions = new String[] { "Transfer-Encoding" };

	private int bufferSize = 1024 * 4;

	public Proxy(int bufferSize) {
		this.bufferSize = bufferSize;
		SSLContext ctx;
		try {
			ctx = SSLContext.getInstance("TLS");
			ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
			SSLContext.setDefault(ctx);
		} catch (Exception e) {
			logger.error("can not initialize proxy", e);
		}
	}

	public void forwardRequest(String server, String targetHost, String path, HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		forwardRequest(server, targetHost, path, req, resp, null);
	}

	/**
	 * Proxies the request
	 * 
	 * @param server         the name of the server
	 * @param targetHost     the target host
	 * @param path           the path
	 * @param req            the HttpServletReqeust
	 * @param resp           the HttpServletResponse
	 * @param headerOverride override the header or not
	 * @throws Exception containing the stacktrace
	 */
	public void forwardRequest(String server, String targetHost, String path, HttpServletRequest req,
			HttpServletResponse resp, Map<String, String> headerOverride) throws Exception {

		String method = req.getMethod();
		// logger.debug("targethost {} method {}", targetHost, method);
		final boolean hasoutbody = (method.equalsIgnoreCase(HttpMethod.POST.name())
				|| method.equalsIgnoreCase(HttpMethod.PUT.name()));
		try {
			byte[] body = null;// StringUtils.EMPTY_STRING;
			if (hasoutbody) {
				if (HttpUtil.isMultipart(req)) {
					// logger.info("it's multipart");
					String contentType = req.getHeader(HttpHeaders.CONTENT_TYPE);
					// logger.info("contentType :" + contentType);
					String[] cc = contentType.split(StringUtils.SEMI_COLON);
					String[] dd = cc[1].split(StringUtils.EQUALS);
					String boundary = StringUtils.TWO_DASHES + dd[1] + StringUtils.CRLF;
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try {
						for (Part part : req.getParts()) {
							baos.write(boundary.getBytes());
							for (String headerName : part.getHeaderNames()) {
								baos.write(
										(headerName + StringUtils.COLON + part.getHeader(headerName) + StringUtils.CRLF)
												.getBytes());
							}
							baos.write((StringUtils.CRLF).getBytes());
							byte[] rr = IOUtils.readFileAsBytes(part.getInputStream());
							baos.write(rr);
							baos.write((StringUtils.CRLF).getBytes());
						}
						// end with a boundary containing extra two dashes
						baos.write((StringUtils.TWO_DASHES + dd[1] + StringUtils.TWO_DASHES + StringUtils.CRLF)
								.getBytes());
						body = baos.toByteArray();
					} 
					catch (ServletException ex) {
						throw new Exception(ex);
					}
				} 
				else {
					body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator())).getBytes();
				}
				// logger.info("body [{}]", new String(body));
			}

			final URL url = new URL(
					targetHost + path + (req.getQueryString() != null ? StringUtils.QUESTION_MARK + req.getQueryString()
							: StringUtils.EMPTY_STRING));
			// logger.debug("url : {}", url);

			HttpURLConnection conn = getConnection(url);
			conn.setRequestMethod(method);

			Enumeration<String> headers = req.getHeaderNames();
			String headerName = null;
			Enumeration<String> values = null;
			String value = null;

			while (headers.hasMoreElements()) {
				headerName = headers.nextElement();
				if (replaceHostname(headerName)) {
					value = req.getHeaders(headerName).nextElement().toString();
					// logger.debug("have to replace hostname {} original value {} ", headerName,
					// value );
					value = replaceServerWithTarget(value, server, targetHost);
					// logger.info("adding connection header {} to {}", headerName, value);
					conn.addRequestProperty(headerName, value);
				} 
				else {
					values = req.getHeaders(headerName);
					while (values.hasMoreElements()) {
						value = values.nextElement();
						// logger.info("adding connection header {} to {}", headerName, value);
						conn.addRequestProperty(headerName, value);
					}
				}

			}

			conn.addRequestProperty(HttpHeaders.HOST, targetHost);
			conn.addRequestProperty(HttpHeaders.XFORWARDED_FOR, server);
			conn.addRequestProperty(HttpHeaders.XFORWARDED_HOST, req.getServerName() + StringUtils.COLON + req.getServerPort());
			conn.addRequestProperty(HttpHeaders.XFORWARDED_PROTO, req.getScheme());
			conn.setRequestProperty(HttpHeaders.ACCEPT_ENCODING, IDENTITY);
			conn.setUseCaches(false);
			// do not follow the redirects
			conn.setInstanceFollowRedirects(false);
			conn.setDoInput(true);

			if (hasoutbody) {
				conn.setDoOutput(hasoutbody);
				conn.getOutputStream().write(body);
			}
			
			int code = conn.getResponseCode();
			// logger.debug("response code {}", code);
			resp.setStatus(conn.getResponseCode());

			String contentType = conn.getContentType();
			// logger.debug("contentType {}", contentType);
			if (StringUtils.isEmpty(contentType)) {
				contentType = req.getHeader(HttpHeaders.CONTENT_TYPE);
			}

			Map<String, List<String>> h = conn.getHeaderFields();
			for (String key : h.keySet()) {
				value = h.get(key).get(0);
				// logger.info("key {} with value {}", key, value);
				if (haveToIncludeHeader(key)) {
					if (key.equalsIgnoreCase(HttpHeaders.LOCATION)) {
						value = value.replace(targetHost, server);
						// logger.debug("location {} value {} after ", key, value);
					}
					// logger.debug("setting header {} with value {}", key, value);
					resp.setHeader(key, value);
				}
			}

			if (headerOverride != null) {
				for (String key : headerOverride.keySet()) {
					resp.setHeader(key, headerOverride.get(key));
				}
			}

			BufferedInputStream input = null;
			if (code < 400) {
				input = new BufferedInputStream(conn.getInputStream());
			}
			else {
				input = new BufferedInputStream(conn.getErrorStream());
			}

			resp.setContentType(conn.getContentType());
			resp.setCharacterEncoding(conn.getContentEncoding());
			resp.setContentLength(conn.getContentLength());
			resp.setHeader(HttpHeaders.CONTENT_LENGTH, StringUtils.EMPTY_STRING + conn.getContentLengthLong());

			byte[] buffer = new byte[bufferSize];
			OutputStream outStream = resp.getOutputStream();
			while (true) {
				int bytesRead = input.read(buffer);
				if (bytesRead < 0)
					break;
				outStream.write(buffer, 0, bytesRead);
			}
			input.close();
			outStream.close();

		}
		catch (Exception e) {
			throw e;
		}
	}

	
	
	private String replaceServerWithTarget(String value, String server, String targetHost) {
		return value.replace(server, targetHost);
	}
	
	

	private boolean replaceHostname(String key) {
		List<String> l = Arrays.asList(inclusions);
		if (key != null) {
			return l.contains(key);
		}
		return false;
	}

	private boolean haveToIncludeHeader(String key) {
		List<String> l = Arrays.asList(exclusions);
		if (key != null) {
			return !l.contains(key);
		}
		return false;
	}
	

	
	
	private HttpURLConnection getConnection(final URL url) throws IOException {
		if (url.getProtocol().equalsIgnoreCase(HttpProtocol.HTTPS.name())) {
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String arg0, SSLSession arg1) {
					return true;
				}
			});
			return conn;
		}
		else {
			return (HttpURLConnection) url.openConnection();
		}
	}
	
	

	private class DefaultTrustManager implements X509TrustManager {

		@Override
		public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
				throws java.security.cert.CertificateException {
			// TODO Auto-generated method stub

		}

		@Override
		public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
				throws java.security.cert.CertificateException {
			// TODO Auto-generated method stub

		}

		@Override
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}
	}

}
