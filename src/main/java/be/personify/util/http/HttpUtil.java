package be.personify.util.http;

import be.personify.util.StringUtils;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Utility class for http stuff
 * 
 * @author vanderw
 *
 */
public class HttpUtil {
	
	

	/**
	 * Gets the full url
	 * 
	 * @param request the HttpServletRequest
	 * @return the string containing the full url
	 */
	public static String getFullURL(HttpServletRequest request) {
		StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
		String queryString = request.getQueryString();
		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append(StringUtils.QUESTION_MARK).append(queryString).toString();
		}
	}
	

	/**
	 * Detects if a request is multipart
	 * @param request the HttpServletRequest
	 * @return a boolean indicating if it is multipart
	 */
	public static boolean isMultipart(HttpServletRequest request) {
		String contentType = request.getContentType();
		if (contentType == null) {
			return false;
		}
		if (contentType.toLowerCase().startsWith(StringUtils.MULTIPART_START)) {
			return true;
		}
		return false;
	}
	
	
	
	public static String detectServerName(HttpServletRequest httpServletRequest) {
		String serverName = httpServletRequest.getServerName(); 
		
		//check forwarded_for
		String forwardedFor = httpServletRequest.getHeader(HttpHeaders.XFORWARDED_FOR);
		if ( !StringUtils.isEmpty(forwardedFor)) {
			serverName = extractServerNameFromForwardedFor(forwardedFor);
		}
		return serverName;
	}


	public static String extractServerNameFromForwardedFor(String forwardedFor) {
		int colonFoundAt = forwardedFor.lastIndexOf(StringUtils.COLON);
		if ( colonFoundAt > 0 ) {
			forwardedFor = forwardedFor.substring(0, colonFoundAt );
			colonFoundAt = forwardedFor.lastIndexOf(StringUtils.COLON_SLASHSLASH);
			if ( colonFoundAt > 0 ) {
				forwardedFor = forwardedFor.substring(colonFoundAt + StringUtils.COLON_SLASHSLASH.length() , forwardedFor.length() );
			}
		}
		return forwardedFor;
	}
	
	
	
	private static final String[] HEADERS_TO_TRY = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR" };

	
	public String getClientIpAddress(HttpServletRequest request) {
	    for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	            return ip;
	        }
	    }
	
	    return request.getRemoteAddr();
	}

}
