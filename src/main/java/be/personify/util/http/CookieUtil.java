package be.personify.util.http;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Class to get and set cookies
 * @author vanderw
 *
 */
public class CookieUtil {
	
	
	/**
	 * Gets a cookie
	 * @param request the HttpServletRequest
	 * @param name the name of the cookie
	 * @return the cookie
	 */
	public static Cookie getCookie( HttpServletRequest request, String name ) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null ) {
			for ( Cookie cookie : cookies ) {
				if ( cookie.getName().equals(name)) {
					return cookie;
				}
			}
		}
		return null;
	}
	
	
	/**
	 * Returns the value of a cookie if present
	 * @param request the HttpServletRequest
	 * @param name the name of the cookie
	 * @return the value of the cookie with the name specified
	 */
	public static String getCookieValue( HttpServletRequest request, String name ) {
		Cookie cookie = getCookie(request, name);
		if ( cookie != null ) {
			return cookie.getValue();
		}
		return null;
	}
	
	
	
	/**
	 * Sets a cookie
	 * @param response the HttpServletResponse
	 * @param name the name of the cookie
	 * @param value the value of the cookie
	 * @param path the patch of the cookie
	 * @param expirySeconds the expiry time of the cookie in seconds
	 * @param secure indicating if it has to be secure
	 * @param domain the domain on which the cookie has to be set
	 */
	public static void setCookie( HttpServletResponse response, String name, String value, String path, int expirySeconds, boolean secure, String domain ) {
		Cookie cookie = new Cookie(name, value);
		cookie.setHttpOnly(true);
		cookie.setPath(path);
		cookie.setMaxAge(expirySeconds);
		cookie.setSecure(secure);
		cookie.setDomain(domain);
		response.addCookie(cookie);
	}

}
