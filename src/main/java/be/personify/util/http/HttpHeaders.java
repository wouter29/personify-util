package be.personify.util.http;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Class to bundle HTTP header names
 * @author vanderw
 *
 */
public class HttpHeaders {
	
	public static final DateFormat DATE_FORMAT_EXPIRES = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss zzz");
 
	public static final String CONTENT_TYPE = "Content-Type";
	
	public static final String CONTENT_LENGTH = "Content-Length";
	
	public static final String USER_AGENT = "User-Agent";
	
	public static final String AUTHORIZATION = "Authorization";
	
	public static final String ACCEPT = "Accept";
	
	public static final String Server = "Server";
	
	public static final String LOCATION = "Location";
	
	public static final String HOST = "Host";
	
	public static final String ACCEPT_ENCODING = "Accept-Encoding";
	
	public static final String XFRANE_OPTIONS = "X-Frame-Options";
	
	public static final String XFORWARDED_FOR = "X-Forwarded-For";
	
	public static final String XFORWARDED_HOST = "X-Forwarded-Host";
	
	public static final String XFORWARDED_PREFIX = "X-Forwarded-Prefix";
	
	public static final String XFORWARDED_PROTO = "X-Forwarded-Proto";
	
	public static final String EXPIRES = "Expires";
	
	public static final String CACHE_CONTROL = "Cache-Control";
	
	public static final String PRAGMA = "Pragma";
	
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	
	
	public static String getCaseInsensitiveHeader( Map<String,String> headers, String key) {
		if ( headers.containsKey(key)) {
			return headers.get(key);
		}
		else {
			if ( headers.containsKey(key.toLowerCase())) {
				return headers.get(key.toLowerCase());
			}
		}
		return null;
	}
	

	
}
