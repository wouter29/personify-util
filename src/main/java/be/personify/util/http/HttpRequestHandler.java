package be.personify.util.http;

import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public abstract class HttpRequestHandler {
	
		
	protected Map<String,String> configuration = null;
	
	private static final Class<?>[] cArg = new Class[] { Map.class };
	
	private static final Map<String,Class<?>> classCache = new HashMap<String,Class<?>>();
	
	public HttpRequestHandler(Map<String,String> configuration) {
		this.configuration = configuration;
	}
	
	public abstract void handle( HttpServletRequest request, HttpServletResponse response ) throws Exception;
	
	public abstract Map<String,String> collectHeaderOverrides();
	
	public static HttpRequestHandler instanciate(String className, Map<String,String> configuration) throws Exception {
		Class<?> clazz = classCache.get(className);
		if (clazz == null ) {
			clazz = Class.forName(className);
			classCache.put(className, clazz);
		}
		return (HttpRequestHandler)clazz.getDeclaredConstructor(cArg).newInstance(configuration);
	}
	

}
