package be.personify.util.http;

/**
 * Utility class to list http methods
 * @author vanderw
 *
 */
public enum HttpMethod {

	GET, 
	POST,  
	PUT,
	DELETE,
	PATCH; 
	
}
	
