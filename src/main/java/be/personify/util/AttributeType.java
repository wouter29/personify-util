package be.personify.util;


public enum AttributeType {

	STRING,INTEGER,BOOLEAN,DATETIME,PASSWORD,MAP,LIST;
	
	public static AttributeType valueOfIgnoreCase( String s ){
		return valueOf(s.toUpperCase());
	}

}
