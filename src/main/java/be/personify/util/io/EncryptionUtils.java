package be.personify.util.io;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;



public class EncryptionUtils {

	
	private static final String AES = "AES";
	private String SECRET_KEY;
	private int GCM_IV_LENGTH = 16;
	private SecretKeyFactory factory = null;
	private Cipher cipher = null;
	private SecureRandom sr = null;

	
	public EncryptionUtils() {
		SECRET_KEY = System.getProperty("encryption.secret.key");
		if ( SECRET_KEY == null) {
			SECRET_KEY = "WHERE ARE THE JEWELS!";
		}
		try {
			sr = SecureRandom.getInstance("SHA1PRNG");
			cipher = Cipher.getInstance("AES/GCM/NoPadding");
			factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new RuntimeException("Can not initialize", e);
		}
	}
	
	
	/**
	 * Constructor
	 * @param value the value
	 */
	public EncryptionUtils( String value ) {
		SECRET_KEY = value;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG");
			cipher = Cipher.getInstance("AES/GCM/NoPadding");
			factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new RuntimeException("Can not initialize", e);
		}
	}
	
	

	/**
	 * Encrypt the thing
	 * @param strToEncrypt the string to encrypt
	 * @param salt the salt
	 * @return the encrypted string
	 */
	public String encrypt(String strToEncrypt, byte[] salt) {
		if (strToEncrypt == null) {
			return null;
		}
		try {
			byte[] iv = new byte[GCM_IV_LENGTH];

			KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), salt, 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), AES);

			GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(IOUtils.CHARSET_UTF_8)));
		} catch (Exception e) {
			throw new RuntimeException("Can not encrypt", e);
		}
	}

	
	
	/**
	 * Decrypt the thing
	 * @param strToDecrypt the string to decrypt
	 * @param salt the salt
	 * @return the decrypted string
	 */
	public String decrypt(String strToDecrypt, byte[] salt) {
		if (strToDecrypt == null) {
			return null;
		}
		try {
			byte[] iv = new byte[GCM_IV_LENGTH];

			KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), salt, 65536, 256);
			SecretKey tmp = factory.generateSecret(spec);
			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), AES);

			GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			throw new RuntimeException("Can not decrypt", e);
		}
	}

	
	/**
	 * Get some salt
	 * @return the salty salt
	 */
	public byte[] getSalt() {
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt;
	}

}
