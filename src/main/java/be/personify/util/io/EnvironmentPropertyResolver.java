package be.personify.util.io;

import org.apache.logging.log4j.core.lookup.StrSubstitutor;

public class EnvironmentPropertyResolver {
	
	    private static final StrSubstitutor envPropertyResolver = new StrSubstitutor(System.getenv());
	    
	    
	    public static String replace(String stringToReplace ) {
	    	return envPropertyResolver.replace(stringToReplace);
	    }

}

