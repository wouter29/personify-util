package be.personify.util.io;

import java.security.MessageDigest;
import java.security.SecureRandom;

public class HashUtils {
	
	
	private static final int FF_HEX_VALUE = 0xFF;
	
	
	/**
	 * Creates a message digester
	 * @return
	 * @throws Exception
	 */
	public static MessageDigest getMessageDigest() throws Exception {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		return getMessageDigest(salt);
	}
	
	
	/**
	 * Creates a messagedigester
	 * @param salt
	 * @return
	 * @throws Exception
	 */
	public static MessageDigest getMessageDigest( byte[] salt ) throws Exception {
		MessageDigest md= MessageDigest.getInstance("SHA-512");
		md.update(salt);
		return md;
	}
	
	
	
	public static String asHex( byte[] hashed ) {
		StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hashed.length; i++) {
            hexString.append(Integer.toHexString(FF_HEX_VALUE & hashed[i]));
        }
        return hexString.toString();
	}
	
	
	

}
