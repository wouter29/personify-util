package be.personify.util.io;

import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncodingUtils {
	
	public static final String DEFAULT_ENCODING = "utf-8";
	
	private static final Logger logger = LoggerFactory.getLogger(EncodingUtils.class);
	
	
	/**
	 * Url encodes a string with the default encoding
	 * @param stringToEncode the string to urlEncode
	 * @return the urlencoded string
	 */
	public static String urlEncode( String stringToEncode ) {
		try {
			return URLEncoder.encode(stringToEncode, DEFAULT_ENCODING);
		}
		catch( Exception e ) {
			logger.error("urlencoding failed for straing {}", stringToEncode, e);
		}
		return stringToEncode;
	}

}
