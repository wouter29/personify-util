package be.personify.util.io;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utility class for IO
 * @author vanderw
 *
 */
public class IOUtils {
	
	
	public static final String CHARSET_UTF_8 = "UTF-8";
	
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	/**
	 * Read a Inputstream
	 * @param inputStream containign the things to read
	 * @return a byte array cintaing the things that are read
	 * @throws IOException containing the stacktrace
	 */
	public static byte[] readFileAsBytes(InputStream inputStream) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    int nbytes = 0;
	    byte[] buffer = new byte[1024*4];

	    try {
	    	if ( inputStream == null) {
	    		throw new IOException("inputstream is null");
	    	}
	        while ((nbytes = inputStream.read(buffer)) != -1) {
	            out.write(buffer, 0, nbytes);
	        }
	        return out.toByteArray();
	    } 
	    finally {
	        if (inputStream != null) { 
	        	inputStream.close();
	        }
	        if (out != null) {
	            out.close();
	        }
	    }    
	}
	
	
	/**
	 * Returns the default charset
	 * @return the default charset
	 */
	public static final Charset defaultCharset() {
		return Charset.forName(CHARSET_UTF_8);
	}
	
	
	/**
	 * Reads a file as bytes
	 * @param c the class for the getresourceasstream
	 * @param fileName the name of the file
	 * @return the byte array containing the things read
	 * @throws IOException containing the stacktrace
	 */
	public static byte[] readFileAsBytes(Class<?> c, String fileName) throws IOException {
	    InputStream inputStream = new BufferedInputStream(c.getClassLoader().getResourceAsStream(fileName));
	    return readFileAsBytes(inputStream);
	}
	
	
	public static String[] readFileAsLines( String fileName ) {
		List<String> lines = new ArrayList<>();
		try {  
			FileInputStream fis=new FileInputStream(fileName);       
			Scanner sc=new Scanner(fis); 
			while(sc.hasNextLine()) {  
				lines.add(sc.nextLine());
			}  
			sc.close();
		}  
		catch(IOException e)  {  
			e.printStackTrace();  
		}
		return lines.toArray(new String[0]);
	}
	
	
	/**
	 * Returns the name/ip
	 * @return string containing the hostname/ip of the localhost
	 */
	public static String getIp() {
		try {
			return InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return "unknown";
	}
	
	
	
	public static void zip(String sourceDirPath, String zipFilePath) throws IOException {
	    Path p = Files.createFile(Paths.get(zipFilePath));
	    Path pp = Paths.get(sourceDirPath);
	    try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p));
	        Stream<Path> paths = Files.walk(pp)) {
	        paths
	          .filter(path -> !Files.isDirectory(path))
	          .forEach(path -> {
	              ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
	              try {
	                  zs.putNextEntry(zipEntry);
	                  Files.copy(path, zs);
	                  zs.closeEntry();
	            } catch (IOException e) {
	                System.err.println(e);
	            }
	          });
	    }
	}
	
	
	
	public static String humanReadableByteCountBin(long bytes) {
	    long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
	    if (absB < 1024) {
	        return bytes + " B";
	    }
	    long value = absB;
	    CharacterIterator ci = new StringCharacterIterator("KMGTPE");
	    for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
	        value >>= 10;
	        ci.next();
	    }
	    value *= Long.signum(bytes);
	    return String.format("%.1f %ciB", value / 1024.0, ci.current());
	}
	
	
	
	
	public static List<String> parseCsvLine( String input ) {
		List<String> result = new ArrayList<String>();
		int start = 0;
		boolean inQuotes = false;
		for (int current = 0; current < input.length(); current++) {
			if (input.charAt(current) == '\"') {
				inQuotes = !inQuotes; // toggle state
			} else if (input.charAt(current) == ',' && !inQuotes) {
				result.add(input.substring(start, current));
				start = current + 1;
			}
		}
		result.add(input.substring(start));
		return result;
		
	}
	

}
