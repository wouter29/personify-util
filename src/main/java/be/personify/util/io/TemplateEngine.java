package be.personify.util.io;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class TemplateEngine {
	
	
	private static final String TEMPLATE_NAME = "templateName";
	public static Configuration cfg;
	
	static {
		cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setDefaultEncoding(IOUtils.CHARSET_UTF_8);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
	}
	
	public static String process( String content, Map<Object,Object> map) throws Exception {
		 Template t = new Template(TEMPLATE_NAME, new StringReader(content), cfg);
		 Writer out = new StringWriter();
		 t.process(map, out);
		 return out.toString();
	}

}
