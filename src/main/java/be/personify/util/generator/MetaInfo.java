package be.personify.util.generator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE, ElementType.FIELD}) //
public @interface MetaInfo {
	
	
	String name() default "none";
	
	int number() default 100;
	
	String description() default "none";
	
	String group() default "default";
	
	String frontendGroup() default "default";
	
	String iconClass() default "barcode";
	
	String security() default "authenticated";
	
	boolean creatable() default true;
	
	boolean editable() default true;
	
	boolean viewable() default true;
	
	boolean deletable() default true;
	
	boolean searchable() default true;
	
	boolean required() default true;
	
	boolean addable() default true;
	
	boolean showInMenu() default false;
	
	boolean showInSearchResultGrid() default true;
	
	boolean showInSearchResultGridMobile() default true;
	
	boolean showOnHomePage() default true;
	
	int sortOrderInGroup() default 1;
	
	boolean workflowEnabled() default false;
	
	boolean isConcept() default false;
	
	String identityDirection() default "none";
	
	String organisationDirection() default "none";
	
	String roleDirection() default "none";
	
	String afterDeleteGoToLink() default "_self";
	
	String afterCreateGoToLink() default "_self";
	
	String afterUpdateGoToLink() default "_self";
	
	boolean enumeration() default false;
	
	boolean rst_include_description() default false;
	
	boolean rst_include_extra() default false;
	
	boolean rst_include_ui() default true;
	
	String rst_diagram_width_percentage() default "70";
	
	String sampleValue() default "sample";
	
	String dependantChoice() default "none";
	
	String sortProperty() default "name";
	
	String sortOrder() default "asc";
	
	String dateFormat() default "dd/MM/yyyy";
	
	String dateHighlighter() default "";
	
	boolean attributeForUniqueness() default false;
	
	boolean uploadable() default false;
	
	boolean showEditButtonOnSearchResult() default true;
	
	boolean showDeleteButtonOnSearchResult() default true;
	
	boolean showDetailButtonOnSearchResult() default true;
	
	String persistentPropertiesToIncludeOnSearchResult() default "";
	
	String persistentPropertiesToIncludeOnEntityPage() default "";
	
	String customRenderer() default "none";
	
	String filter() default "";
	
	boolean secret() default false;
	
	String tableName() default "";
	
	String validationPattern() default "";
	
	String fixedInitialValue() default "";
	
	boolean dashboard() default false;
	
	String dashboardField() default "";
	
	boolean dashboardExclude() default false;
	
	String dashboardSubtypeDirection() default "";
	
	String unit() default "";
	
	boolean auditable() default false;
	
	boolean includeInAPIDefinition() default false;
	
	String mappedBy() default "";
	
	boolean unidirectional() default false;
	
	String displayNameFields() default "";
	
	

}
