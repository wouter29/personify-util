package be.personify.util;

public interface PolicyEvaluator {
		
	public boolean hasPermissionForResource( String resourceType ) ;
	
	public boolean hasPermissionForResources( String ... resourceTypes );
	
	public boolean evaluate( Decision decision, Action action, String resourceType );
	
	public boolean evaluate( Decision decision, Action action, String resourceType, Object object );	
	
}
