package be.personify.util.provisioning;

public class TargetSystemAttributeMapping {

	private String id;
	private String localName;
	private String remoteName;
	private TargetSystem targetSystem;
	private TargetSystemAttribute targetSystemAttribute; 
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public TargetSystemAttribute getTargetSystemAttribute() {
		return targetSystemAttribute;
	}

	public void setTargetSystemAttribute(TargetSystemAttribute targetSystemAttribute) {
		this.targetSystemAttribute = targetSystemAttribute;
	}

	public TargetSystem getTargetSystem() {
		return targetSystem;
	}

	public void setTargetSystem(TargetSystem targetSystem) {
		this.targetSystem = targetSystem;
	}
	
	public String getRemoteName() {
		return remoteName;
	}

	public void setRemoteName(String remoteName) {
		this.remoteName = remoteName;
	}
	
}
