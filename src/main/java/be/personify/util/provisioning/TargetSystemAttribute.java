package be.personify.util.provisioning;

import com.fasterxml.jackson.annotation.JsonIgnore;

import be.personify.util.AttributeType;


public class TargetSystemAttribute {

	private String id;
	private String name;
	
	@JsonIgnore
	private TargetSystem targetSystem; 
	private AttributeType attributeType;
	private boolean readable;
	private boolean writable;
	private boolean indexed;
	private boolean unique;
	private boolean multiValued;
	private boolean primaryKey;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AttributeType getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(AttributeType attributeType) {
		this.attributeType = attributeType;
	}

	public boolean isReadable() {
		return readable;
	}

	public void setReadable(boolean readable) {
		this.readable = readable;
	}

	public boolean isWritable() {
		return writable;
	}

	public void setWritable(boolean writable) {
		this.writable = writable;
	}

	public boolean isMultiValued() {
		return multiValued;
	}

	public void setMultiValued(boolean multiValued) {
		this.multiValued = multiValued;
	}


	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	public TargetSystem getTargetSystem() {
		return targetSystem;
	}

	public void setTargetSystem(TargetSystem targetSystem) {
		this.targetSystem = targetSystem;
	}
	
	
	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}
	
	
	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	
	
	
	
}
