package be.personify.util.provisioning;

import java.util.Map;

import be.personify.util.ConnectorType;


public class ConnectorConfiguration {
	

	private String id;
	private String name;
	private String description;
	private ConnectorType connectorType;
	private String customConnectorClassName;
	private int poolSize = 1;
	private int maxPoolValidity = 3600;
	private Map<String,String> configuration;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

	public ConnectorType getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(ConnectorType connectorType) {
		this.connectorType = connectorType;
	}

	public String getCustomConnectorClassName() {
		return customConnectorClassName;
	}

	public void setCustomConnectorClassName(String customConnectorClassName) {
		this.customConnectorClassName = customConnectorClassName;
	}
	
	public int getMaxPoolValidity() {
		return maxPoolValidity;
	}

	public void setMaxPoolValidity(int maxPoolValidity) {
		this.maxPoolValidity = maxPoolValidity;
	}

	

	

}
