package be.personify.util.provisioning;

import be.personify.util.Action;
import be.personify.util.State;

public class ProvisionDecision {
	
	
	private String id;
	
	private State remoteState;
	
	private State localState;
	
	private Action action;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public State getRemoteState() {
		return remoteState;
	}

	public void setRemoteState(State remoteState) {
		this.remoteState = remoteState;
	}

	public State getLocalState() {
		return localState;
	}

	public void setLocalState(State localState) {
		this.localState = localState;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}



}
