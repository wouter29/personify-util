package be.personify.util.provisioning;

import java.util.List;
import java.util.Map;

import be.personify.util.JoinInfo.Join;

public class TargetSystem {
	
	private String id;
	private String name;
	private String code;
	private String description;
	private boolean active;
	private boolean updateIfIdentical;
	private boolean supportsPagination;
	private boolean supportsUserPassword;
	private boolean pingEnabled;
	private boolean notifyOnFailedPing;
	private List<TargetSystemAttribute> targetSystemAttributes;
	private List<TargetSystemAttributeMapping> targetSystemAttributeMappings;
	private List<ProvisionDecision> provisionDecisions;
	private ConnectorConfiguration connectorConfiguration;
	private AccountIdGenerator accountIdGenerator;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	
	public ConnectorConfiguration getConnectorConfiguration() {
		return connectorConfiguration;
	}

	public void setConnectorConfiguration(ConnectorConfiguration connectorConfiguration) {
		this.connectorConfiguration = connectorConfiguration;
	}


	public List<TargetSystemAttribute> getTargetSystemAttributes() {
		return targetSystemAttributes;
	}

	public void setTargetSystemAttributes(List<TargetSystemAttribute> targetSystemAttributes) {
		this.targetSystemAttributes = targetSystemAttributes;
	}

	public List<ProvisionDecision> getProvisionDecisions() {
		return provisionDecisions;
	}

	public void setProvisionDecisions(List<ProvisionDecision> provisionDecisions) {
		this.provisionDecisions = provisionDecisions;
	}

	public boolean isUpdateIfIdentical() {
		return updateIfIdentical;
	}

	public void setUpdateIfIdentical(boolean updateIfIdentical) {
		this.updateIfIdentical = updateIfIdentical;
	}

	public AccountIdGenerator getAccountIdGenerator() {
		return accountIdGenerator;
	}
	
	public boolean isSupportsPagination() {
		return supportsPagination;
	}

	public void setSupportsPagination(boolean supportsPagination) {
		this.supportsPagination = supportsPagination;
	}

	public void setAccountIdGenerator(AccountIdGenerator accountIdGenerator) {
		this.accountIdGenerator = accountIdGenerator;
	}

	public TargetSystemAttribute calculatePrimaryKeyAttribute() {
		for ( TargetSystemAttribute attribute : getTargetSystemAttributes()) {
			if ( attribute.isPrimaryKey()) {
				return attribute;
			}
		}
		throw new RuntimeException("the primary key attribute is not found for targetsystem [" + getName() + "]");
	}

	public boolean isPingEnabled() {
		return pingEnabled;
	}

	public void setPingEnabled(boolean pingEnabled) {
		this.pingEnabled = pingEnabled;
	}

	public List<TargetSystemAttributeMapping> getTargetSystemAttributeMappings() {
		return targetSystemAttributeMappings;
	}

	public void setTargetSystemAttributeMappings(List<TargetSystemAttributeMapping> targetSystemAttributeMappings) {
		this.targetSystemAttributeMappings = targetSystemAttributeMappings;
	}
	
	

	public boolean isSupportsUserPassword() {
		return supportsUserPassword;
	}

	public void setSupportsUserPassword(boolean supportsUserPassword) {
		this.supportsUserPassword = supportsUserPassword;
	}
	
	

	public boolean isNotifyOnFailedPing() {
		return notifyOnFailedPing;
	}

	public void setNotifyOnFailedPing(boolean notifyOnFailedPing) {
		this.notifyOnFailedPing = notifyOnFailedPing;
	}
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	

	

	@Override
	public String toString() {
		return "TargetSystem [id=" + id + ", name=" + name + ", code=" + code + ", description=" + description
				+ ", active=" + active + ", updateIfIdentical=" + updateIfIdentical + ", supportsPagination="
				+ supportsPagination + ", supportsUserPassword=" + supportsUserPassword + ", pingEnabled=" + pingEnabled
				+ ", notifyOnFailedPing=" + notifyOnFailedPing + ", targetSystemAttributes=" + targetSystemAttributes
				+ ", targetSystemAttributeMappings=" + targetSystemAttributeMappings + ", provisionDecisions="
				+ provisionDecisions + ", connectorConfiguration=" + connectorConfiguration + ", accountIdGenerator="
				+ accountIdGenerator + "]";
	}

	
	
	
	
	

	

}
