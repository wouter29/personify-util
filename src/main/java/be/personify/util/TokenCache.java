package be.personify.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenCache {

	
	private static TokenCache INSTANCE = new TokenCache();
	
	private static final Logger logger = LoggerFactory.getLogger(TokenCache.class);
	
	private Map<String,Map<String,String>> tokenInfoMap = new HashMap<String,Map<String,String>>();
	
	private long lastTimeChecked = System.currentTimeMillis();
	private long checkInterval = 1000L * 60 * 1;//every 1 minute
	
	private static final String VALID_TO_KEY = "validTo";
	
	/**
	 * The instance
	 * @return the instance of the tokencache
	 */
	public static TokenCache getInstance() {
		return INSTANCE;
	}
	
	
	
	public void storeTokenInfo( String token, Map<String,String> tokenInfo) {
		tokenInfoMap.put(token, tokenInfo);
	}
	
	
	public Map<String,String> getTokenInfo( String token ) {
		checkMap();
		return tokenInfoMap.get(token);
	}
	
	
	private void checkMap() {
		if ( System.currentTimeMillis() > lastTimeChecked + checkInterval ) {
			logger.debug("checking invalid tokens");
			Map<String,Map<String,String>> freshMap = new HashMap<String,Map<String,String>>();
			for (Map.Entry<String, Map<String,String>> entry : tokenInfoMap.entrySet())	{
			    if ( Long.parseLong(entry.getValue().get(VALID_TO_KEY)) > System.currentTimeMillis() ) {
			    	freshMap.put(entry.getKey(), entry.getValue());
			    }
			    else {
			    	logger.info("expiring invalid token {}", entry.getKey());
			    }
			}
			lastTimeChecked = System.currentTimeMillis();
			tokenInfoMap = freshMap;
			
		}
		
	}
	
}
