package be.personify.util.sms;

public interface SmsSender {
	
	
	public boolean send(final String from, final String to, final String message) ;
	
	
}
