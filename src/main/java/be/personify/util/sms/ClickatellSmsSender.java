package be.personify.util.sms;


import java.io.StringWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import be.personify.util.StringUtils;
import be.personify.util.http.HttpHeaders;

public class ClickatellSmsSender implements SmsSender {

	private static final String ERROR = "error";
	private static final String ACCEPTED = "accepted";

	private static final Logger logger = LoggerFactory.getLogger(ClickatellSmsSender.class);

    private final String token;
    private final String serverUrl;

    private final ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
    private final RestTemplate restTemplate = new RestTemplate();
    
    

    public ClickatellSmsSender( String token,  String serverUrl) {
        this.token = token;
        this.serverUrl = serverUrl;

        mapper.findAndRegisterModules();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    @Override
    public boolean send(final String from, final String to, final String message) {
        try {
            final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add(HttpHeaders.AUTHORIZATION, this.token);
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

            final Map<String, Object> map = new HashMap<>();
            map.put("content", message);
            map.put("to", new String[]{ to });
            //map.put("from", from);

            final StringWriter stringify = new StringWriter();
            mapper.writeValue(stringify, map);
            
            logger.info("message {}", stringify.toString());

            final HttpEntity<String> request = new HttpEntity<>(stringify.toString(), headers);
            final ResponseEntity<Map> response = restTemplate.postForEntity(new URI(this.serverUrl), request, Map.class);
            if (response.hasBody()) {
                final List<Map> messages = (List<Map>) response.getBody().get("messages");

                final String error = (String) response.getBody().get(ERROR);
                if (!StringUtils.isEmpty(error)) {
                    logger.error(error);
                    return false;
                }

                final List<String> errors = messages.stream()
                        .filter(m -> m.containsKey(ACCEPTED) && !Boolean.parseBoolean(m.get(ACCEPTED).toString()) && m.containsKey(ERROR))
                        .map(m -> (String) m.get(ERROR))
                        .collect(Collectors.toList());
                if (errors.isEmpty()) {
                    return true;
                }
                
            }
        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }
}
