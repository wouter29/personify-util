package be.personify.util;

import java.util.Arrays;
import java.util.List;

public class BooleanUtil {
	
	public static final List<Boolean> choices = Arrays.asList(Boolean.TRUE, Boolean.FALSE);
	
	/**
	 * chaches booleans as a list : for what is this used?
	 * @return a list containing the possible values of a boolean, LOL
	 */
	public static List<Boolean> getBooleansAsList() {
		return choices;
	}
 
}
