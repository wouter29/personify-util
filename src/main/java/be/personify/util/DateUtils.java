package be.personify.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	private static final String Z = "Z";

	public static final DateFormat DATETIMEFORMAT_ISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");	
	
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	public static String format( Date date ) {
		if ( date != null) {
			return DATETIMEFORMAT_ISO.format(date);
		}
		return StringUtils.EMPTY_STRING;
	}
	
	
	public static Date parse( String s ) throws ParseException {
		if ( s != null ) {
			if ( s.endsWith(Z)) {
				return DATE_FORMAT.parse(s);
			}
			else {
				return DATETIMEFORMAT_ISO.parse(s);
			}
		}
		return null;
	}
	
	
}
