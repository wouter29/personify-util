package be.personify.util;

public enum WorkflowState {
	
	STARTED,
	TO_BE_PROCESSED,
	PROCESSING,
	FINISHED;

}
