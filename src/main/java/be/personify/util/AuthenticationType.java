package be.personify.util;

public enum AuthenticationType {
	
	OAUTH,
	BASIC,
	NONE

}
