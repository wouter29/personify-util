package be.personify.util.properties.provider;

import java.util.Locale;
import java.util.Map;

public abstract class RolePropertyProvider {
	
	public Map<String,String> configuration;
	
	public RolePropertyProvider( Map<String,String> configuration ) {
		try {
			validateConfiguration(configuration);
		}
		catch ( Exception e ) {
			throw new RuntimeException(e.getMessage());
		}
		this.configuration = configuration;
	}
	
	public abstract Map<String,String> getProperties( RolePropertyFilter filter, Locale locale);
	
	public abstract void validateConfiguration(Map<String, String> configuration) throws Exception;

	
}
