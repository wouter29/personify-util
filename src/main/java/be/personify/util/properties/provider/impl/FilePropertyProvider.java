package be.personify.util.properties.provider.impl;

import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import be.personify.util.CsvFileReader;
import be.personify.util.StringUtils;
import be.personify.util.properties.provider.RolePropertyFilter;
import be.personify.util.properties.provider.RolePropertyProvider;

/**
 * 
 * @author vanderbw
 * 
 * Provider to give properties to entitlements based on a file
 *
 */
public class FilePropertyProvider extends RolePropertyProvider {

	
	public static final String KEY_FILE = "file";
	
	private static final Logger logger = LogManager.getLogger(FilePropertyProvider.class);
	

	/**
	 * Constructor
	 * 
	 * @param configuration
	 */
	public FilePropertyProvider(Map<String, String> configuration) {
		super(configuration);
	}
	

	@Override
	public Map<String, String> getProperties( RolePropertyFilter filter, Locale locale) {
		
		
		logger.info("getting properties for locale {} with filter {}", locale, filter);
		Map<String,String> p = new TreeMap<String, String>();
		
		//the file
		File f = new File(configuration.get(KEY_FILE));
		if ( !f.exists() || !f.isFile()) {
			logger.error("file {} is not a valid file location", f.getAbsolutePath());
			return p;
		}
		
		//the locale
		if ( locale == null ) {
			locale = new Locale("en");
		}
		
		List<CSVRecord> csvRecords;
		logger.info("file {} exists and is readable", f.getAbsolutePath());
		try {
			csvRecords = CsvFileReader.readCsvFile(new FileReader(f), CSVFormat.DEFAULT);
			if ( csvRecords != null && csvRecords.size() > 0) {
				for ( CSVRecord record : csvRecords ) {
					if ( record.size() == 5) {
						if ( !StringUtils.isEmpty(record.get(2))){
							logger.info("organisation filter is applicable {}", record.get(2));
							if ( !record.get(2).equals(filter.get(RolePropertyFilter.KEY_ORGANISATION_CODE))) {
								break;
							}
						}
						if ( !StringUtils.isEmpty(record.get(3))){
							logger.info("entitlement filter is applicable {}", record.get(3));
							if ( !record.get(3).equals(filter.get(RolePropertyFilter.KEY_ENTITLEMENT_CODE))) {
								break;
							}
						}
						if ( !StringUtils.isEmpty(record.get(4))){
							logger.info("locale filter is applicable {}", record.get(4));
							if ( !new Locale(record.get(4)).getLanguage().equals(locale.getLanguage())) {
								break;
							}
						}
						
						
						p.put(record.get(0), record.get(1));
						
					}
				}
			}
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("can not read csv file", e);
		}
		logger.info("returning properties size {}", p.size());
		
		return p;
	}

	@Override
	public void validateConfiguration(Map<String, String> configuration) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
}
