package be.personify.util.properties.provider.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import be.personify.util.properties.provider.RolePropertyFilter;
import be.personify.util.properties.provider.RolePropertyProvider;

public class StaticPropertyProvider extends RolePropertyProvider {


	public StaticPropertyProvider(Map<String, String> configuration) {
		super(configuration);
	}

	@Override
	public Map<String, String> getProperties( RolePropertyFilter filter, Locale locale) {
		Map<String,String> p = new HashMap<String, String>();
		for ( String key : configuration.keySet()) {
			p.put(key, configuration.get(key));
		}
		return p;
	}

	@Override
	public void validateConfiguration(Map<String, String> configuration) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
}
