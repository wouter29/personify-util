package be.personify.util.properties.provider.impl;

import java.io.IOException;
import java.net.URI;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import be.personify.util.StringUtils;
import be.personify.util.properties.provider.RolePropertyFilter;
import be.personify.util.properties.provider.RolePropertyProvider;

public class RoleAdministratorPropertyProvider extends RolePropertyProvider {

	private static final String ACCESS_TOKEN = "access_token";
	private static final String NAME = "name";
	private static final String CODE = "code";
	private static final String _LINKS = "_links";
	
	public static final String CONFIGURATION_KEY_AUTHORIZATION_URL = "authorizationUrl";
	public static final String CONFIGURATION_KEY_SECRET = "secret";
	public static final String CONFIGURATION_KEY_CLIENTID = "clientid";
	public static final String CONFIGURATION_KEY_ENTITLEMENT_URL = "roleUrl"; 

	private static final Logger logger = LogManager.getLogger(RoleAdministratorPropertyProvider.class);
	
	private static final String BODY_KEY_PASSWORD = "password";
	private static final String BODY_KEY_USERNAME = "username";
	private static final String BODY_KEY_GRANT_TYPE = "grant_type";
	private static final String GRANT_TYPE = "client_credentials";
	
		
	
	public RoleAdministratorPropertyProvider(Map<String, String> configuration) {
		super(configuration);
	}



	@Override
	public Map<String, String> getProperties(RolePropertyFilter filter, Locale locale) {
		RestTemplate restTemplate = getRestTemplate();
		Map<String,String> properties  = new HashMap<String,String>();
		try {
			URI uri = new URI((String)configuration.get(CONFIGURATION_KEY_ENTITLEMENT_URL));
			Map filterMap = new HashMap<>();
			ResponseEntity<Map> result = restTemplate.exchange( uri, HttpMethod.POST, new HttpEntity<Map>(filterMap), Map.class );
			Map embedded = (Map)result.getBody().get("_embedded");
			List<Map> entitlements = (List)embedded.get("role");
			logger.info("roles -> {}", entitlements);
			Map links = null;
			for ( Map entitlement : entitlements ) {
				links = (Map)entitlement.get(_LINKS);
				logger.info("links {}", links);
				properties.put((String)entitlement.get(CODE), (String)entitlement.get(NAME));
			}
			return properties;
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory() {
				
			@Override
			public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
				ClientHttpRequest request = super.createRequest(uri, httpMethod);
				Map tokenResponse = getTokenResponse();
				if ( tokenResponse != null ) {
					String token = (String)tokenResponse.get(ACCESS_TOKEN);
					logger.info("got token {}", token);
					request.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
					return request;
				}
				else {
					throw new RuntimeException("no token found");
				}
			}
			    	
		};
			
		restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
			
		return restTemplate;
	}
	
	
	
	
	public Map getTokenResponse() {
		
			String clientid = (String)configuration.get(CONFIGURATION_KEY_CLIENTID);
			String secret = (String)configuration.get(CONFIGURATION_KEY_SECRET);
			String authUrl = (String)configuration.get(CONFIGURATION_KEY_AUTHORIZATION_URL);
			long start = System.currentTimeMillis();
			logger.debug("fetching a token for clientid {}, using url {}", clientid, authUrl);
			
			//make basic authentication
			String authorization = Base64.getEncoder().encodeToString(( (clientid + StringUtils.COLON + secret).getBytes()));
			logger.debug("authorization constructed");
			
			//make headers
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set(HttpHeaders.AUTHORIZATION, "Basic " + authorization );
			requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());
			logger.debug("headers ready");
			
			//make body
			MultiValueMap<String, String> body = new LinkedMultiValueMap<String,String>();
			body.add(BODY_KEY_GRANT_TYPE, GRANT_TYPE); 
			body.add(BODY_KEY_USERNAME, clientid);
			body.add(BODY_KEY_PASSWORD, secret);
			
			

			//send
			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(body, requestHeaders);
			try {
				ResponseEntity<Map> response = new RestTemplate().exchange(authUrl, HttpMethod.POST, requestEntity, Map.class);
				//tokenResponse.setAccess_token((String)response.getBody().get("access_token"));
				//tokenResponse.setExpires_in((Integer)response.getBody().get("expires_in"));
				return response.getBody();
			}
			catch ( HttpStatusCodeException exception) {
			    int statusCode = exception.getStatusCode().value();
			    logger.error("http error {} {}", statusCode, exception.getResponseBodyAsString());
			}
			catch ( Exception exception) {
				logger.error("can not request token {}", exception);
			}
			
			logger.info("fetched a token for {} in {} ms", clientid, (System.currentTimeMillis() - start));
			
			return null;

	}









	@Override
	public void validateConfiguration(Map<String, String> configuration) throws Exception {
		
		String clientid = configuration.get(CONFIGURATION_KEY_CLIENTID);
		String secret = configuration.get(CONFIGURATION_KEY_SECRET);
		String authUrl = configuration.get(CONFIGURATION_KEY_AUTHORIZATION_URL);
		
		if ( StringUtils.isEmpty(clientid)) {
			throw new Exception("clientid is empty");
		}
		
		if ( StringUtils.isEmpty(secret)) {
			throw new Exception("secret is empty");
		}
		
		if ( StringUtils.isEmpty(authUrl)) {
			throw new Exception("authorizationUrl is empty");
		}
		
	}
	
	
	

}
