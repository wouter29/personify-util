package be.personify.util;

public enum SortOrder {
	
	ascending("asc"), 
	descending("desc");
	
	private final String shortName;
	
	SortOrder( String shortName){
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}
	
	
	
}
