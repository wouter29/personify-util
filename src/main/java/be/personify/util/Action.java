package be.personify.util;

public enum Action {

	ALL,
	DO_NOTHING,
	UNCERTAIN,
	SEARCH,
	READ,
	CREATE, 
	DELETE, 
	UPDATE,
	ARCHIVE,
	UNARCHIVE,
	DISABLE,
	ENABLE;
}
