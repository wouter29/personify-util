package be.personify.util.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import be.personify.util.CsvFileReader;
import be.personify.util.StringUtils;
import be.personify.util.io.IOUtils;

public class JDNData {
	
	List<CSVRecord> csvRecords = null;
	
	private static final String folder = "/home/vandew19/Documents/projecten/jdn";
		
	
	public static void main(String[] args) {
		JDNData csvToData = new JDNData();
		try {
			csvToData.readFile();
			csvToData.generate();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	private void readFile() throws Exception {
		File f = new File(folder,"csvroles.csv");
		csvRecords = CsvFileReader.readCsvFile(new FileReader(f), CSVFormat.DEFAULT);
	}

	
	
	private void generate(){
		//ENT0002,Safety Inspector,Inspector of all things for safety,"{}",12,true,false,1,5
		StringBuffer roleOut = new StringBuffer();
		Integer[] profiles = new Integer[] {4,5,6,7,8,9, 10,11,14,15};
		String[] profileDesc = new String[profiles.length];
		String currentRole = "";
		int roleCount = 0;
		int adgroupCount = 0;
		int adgroupRoleCount = 0;
		List<String> allRoles = new ArrayList<String>(); 
		Map<Integer,List<String>> profileRoles = new HashMap<Integer, List<String>>();
		int recordCount = 0;
		Map<String,String> roleGroupCount = new HashMap<String, String>();
		int envFound = 0;
		try {
			if ( csvRecords != null && csvRecords.size() > 0) {
				
				StringBuffer rolesToProfile = new StringBuffer();
				
				Map<Integer,String> r2p = null;
				
				for ( CSVRecord record : csvRecords ) {
					if ( recordCount == 0 ) {
						int profileCount = 0;
						for ( Integer profileNumber : profiles ) {
							profileDesc[profileCount] = record.get(profileNumber);
							profileCount++;
						}
						recordCount++;
						
						rolesToProfile.append("").append(StringUtils.COMMA);
						for ( String sss : profileDesc) {
							rolesToProfile.append(sss).append(StringUtils.COMMA);
						}
						rolesToProfile.append(IOUtils.LINE_SEPARATOR);
					}
					else if ( recordCount > 3 ) {
						
						String role = record.get(0);
						if ( !StringUtils.isEmpty(role)) {
							roleGroupCount.put(currentRole, "" + adgroupRoleCount);
							adgroupRoleCount = 0;
							if ( r2p != null) {
								
								
								rolesToProfile.append(currentRole).append(StringUtils.COMMA);
								for ( Integer profileNumber : profiles ) {
									String r = r2p.get(profileNumber);
									if ( r != null ) {
										rolesToProfile.append(r).append(StringUtils.COMMA);
									}
									else {
										rolesToProfile.append("").append(StringUtils.COMMA);
									}
								}
								rolesToProfile.append(IOUtils.LINE_SEPARATOR);
							}
							 
							currentRole = role;
							//System.out.println(currentRole);
							roleCount++;
							allRoles.add(currentRole);
							r2p = new HashMap<Integer, String>();
							//rolesToProfile.append(currentRole).append(StringUtils.COMMA);
						}
						else {
							adgroupCount++;
							adgroupRoleCount++;
							
							if ( record.get(3).endsWith("_ACC")) {
								envFound++;
							}
							for ( Integer profileNumber : profiles ) {
								String s = record.get(profileNumber);
								//System.out.println(s);
								if ( s.equalsIgnoreCase("x")) {
									List<String> roles = profileRoles.get(profileNumber);
									if ( roles == null) {
										roles = new ArrayList<String>();
									}
									if ( !roles.contains(currentRole)) {
										roles.add(currentRole);
									}
									profileRoles.put(profileNumber, roles);
									r2p.put(profileNumber, "x");
									//rolesToProfile.append("x").append(StringUtils.COMMA);
								}
								else {
									String ss =  r2p.get(profileNumber) ;
									if ( ss != null && !ss.equals("x")) {
										r2p.put(profileNumber, "");
									}
								}
							}
						}
						recordCount++;
					}
					else {
						recordCount++;
					}
				}
				
				System.out.println("number of roles : " + roleCount);
				System.out.println("number of AD groups : " + adgroupCount);
				
				System.out.println("-----------------------------------");
				
				int c = 0;
				for ( Integer profileNumber : profiles ) {
					List<String> roles = profileRoles.get(profileNumber);
					System.out.println("profiel " + profileNumber + " " + profileDesc[c] + " with " + roles.size() + " roles");
					//System.out.println("-----------------------------------");
					for ( String r : roles ) {
						//System.out.println(r);
					}
					c++;
				}
				
				
				System.out.println("-----------------------------------");
				
				
				int matchCount = 0;
				int smallMatchCount = 0;
				int tinyMatchCount = 0;
				StringBuffer allRolesBuffer = new StringBuffer("");
				StringBuffer smallRolesBuffer = new StringBuffer("");
				StringBuffer tinyRolesBuffer = new StringBuffer("");
				for ( String role : allRoles ) {
					int matches = 0;
					for ( Integer profileNumber : profiles ) {
						List<String> roles = profileRoles.get(profileNumber);
						if ( roles.contains(role)) {
							matches++;
						}
					}
					if ( matches == profiles.length ) {
						allRolesBuffer.append(role + " | ");
						matchCount++;
					}
					else if ( matches < (profiles.length / 5))  {
						tinyRolesBuffer.append(role + " | ");
						tinyMatchCount++;
					}
					else if ( matches < (profiles.length / 2))  {
						smallRolesBuffer.append(role + " | ");
						smallMatchCount++;
					}
					
				}
				System.out.println("everybody has these " + matchCount + " roles : " + allRolesBuffer.toString());
				System.out.println("less then 50% has these " + smallMatchCount + " roles : " + smallRolesBuffer.toString());
				System.out.println("less then 20% has these " + tinyMatchCount + " roles : " + tinyRolesBuffer.toString());
				
				//FileWriter assout = new FileWriter(new File("/tmp/ROLE.csv"));
				//assout.write(roleOut.toString());
				//assout.flush();
				
				
				StringBuffer b = new StringBuffer("");
				for ( String key : roleGroupCount.keySet()) {
					if ( !StringUtils.isEmpty(key)) {
						b.append(key + "," + roleGroupCount.get(key)).append(IOUtils.LINE_SEPARATOR);
					}
				}
				FileWriter w = new FileWriter(new File(folder, "profileCountPerRole.csv"));
				w.write(b.toString());
				w.flush();
				
				
				w = new FileWriter(new File(folder, "rolesToProfile.csv"));
				w.write(rolesToProfile.toString());
				w.flush();
				
				System.out.println("environmental AD context " + envFound);
				
			}
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
	}
	
	
	
	

}
