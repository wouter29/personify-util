package be.personify.util.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import be.personify.util.CsvFileReader;
import be.personify.util.StringUtils;
import be.personify.util.io.IOUtils;

public class CsvToData {
	
	List<CSVRecord> csvRecords = null;
		
	
	public static void main(String[] args) {
		CsvToData csvToData = new CsvToData();
		try {
			csvToData.readFile();
			Map<String,String> users = csvToData.generateUsersAndAssignments();
			System.out.println("users : " + users.size());
			Map<String,RoleWrapper> roles = csvToData.generateRoles();
			csvToData.generateContextFiles(roles);
			csvToData.generateRoleAssignments(users, roles);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	private void readFile() throws Exception {
		File f = new File("/home/vandew19/Documents/projecten/jdn/rollen_finance.csv");
		csvRecords = CsvFileReader.readCsvFile(new FileReader(f), CSVFormat.DEFAULT);
	}

	
	
	
	
	
	private void generateRoleAssignments( Map<String,String> users, Map<String,RoleWrapper> roles ) throws Exception {
		//0000000001,SNPP,ENT0001,Homer windows account
		StringBuffer out = new StringBuffer();
		for ( String user : users.keySet() ) {
			Map<String,RoleWrapper> userRoles = new HashMap<String, CsvToData.RoleWrapper>();
			try {
				if ( csvRecords != null && csvRecords.size() > 0) {
					int count = 1;
					for ( CSVRecord record : csvRecords ) {
						String name = record.get(5);
						if (!name.equals("User") && name.equals(user) ) {
							String role = record.get(4);
							RoleWrapper r = getRoleByName(role, roles);
							String context = record.get(3);
							if ( userRoles.containsValue(r.getCode())) {
								if ( !userRoles.get(role).getContexts().contains(context) ) {
									//
								}
							}
							else {
								userRoles.put(r.getCode(),r);
							}
						}
					}
				}
				System.out.println("user " + user + " has " + userRoles.size() );
				for ( String role : userRoles.keySet()) {
					System.out.println(users.get(user) + "----------" + role);
					out.append(users.get(user)).append(StringUtils.COMMA).append("FIN").append(StringUtils.COMMA).append(role).append(IOUtils.LINE_SEPARATOR);
				}
			}
			catch ( Exception e ) {
				e.printStackTrace();
			}
		}
		
		FileWriter rout = new FileWriter(new File("/tmp/ROLEASSIGNMENT.csv"));
		rout.write(out.toString());
		rout.flush();
		
		//System.err.println(out.toString());
	}
	
	
	
	private RoleWrapper getRoleByName(String name, Map<String,RoleWrapper> roles ) {
		//System.err.println("nr  of roles : " + roles.size());
		for ( String n : roles.keySet() ) {
			//System.out.println("n : " + n + " ---- " + roles.get(n).getName());
			if ( roles.get(n).getName().equals(name)) {
				return roles.get(n);
			}
		}
		System.out.println("role with name : " + name + " not found");
		return null;
	}
	
	
	
	private Map<String,RoleWrapper> generateRoles(){
		//ENT0002,Safety Inspector,Inspector of all things for safety,"{}",12,true,false,1,5
		Map<String,List<String>> roles = new HashMap<String, List<String>>();
		Map<String,RoleWrapper> newRoles = new HashMap<String,RoleWrapper>();
		StringBuffer roleOut = new StringBuffer();
		try {
			if ( csvRecords != null && csvRecords.size() > 0) {
				int count = 1;
				for ( CSVRecord record : csvRecords ) {
					String name = record.get(4);
					
					String context = record.get(3);
					if ( !record.get(5).equals("User") && !StringUtils.isEmpty(name) ) {
						String roleName = "ROLE0" + count;
						System.out.println(name + "----------" + roleName + "---" + record); 
						if ( !roles.containsKey(name)) {
							System.out.println("not contains");
							roleOut.append(roleName).append(StringUtils.COMMA);
							roleOut.append(name).append(StringUtils.COMMA);
							roleOut.append(name).append(StringUtils.COMMA);
							roleOut.append("\"{}\",12,true,false,1,5").append(StringUtils.COMMA);
							roleOut.append(IOUtils.LINE_SEPARATOR);
							count++;
							List<String> contexts = new ArrayList<String>();
							contexts.add(context);
							roles.put(name, contexts);
							RoleWrapper wrapper = new RoleWrapper(name, roleName);
							wrapper.setContexts(contexts);
							newRoles.put(roleName, wrapper);
						}
						else {
							System.out.println("contains");
							List<String> contexts = roles.get(name);
							if ( !contexts.contains(context)) {
								contexts.add(context);
								roles.put(name, contexts);
								RoleWrapper w = getRoleByName(name, newRoles);
								newRoles.get(w.getCode()).setContexts(contexts);
							}
							
						}
					}
				}
				
				FileWriter assout = new FileWriter(new File("/tmp/ROLE.csv"));
				assout.write(roleOut.toString());
				assout.flush();
			}
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		return newRoles;
	}
	
	
	private Map<String,String> generateUsersAndAssignments(){
		//Homer,Simpson,M,0000000001,,,HUMAN
		//Active,Application,,JdnTime,Normal User,Levrier Piet,N,,,
		
		Map<String,String> users = new HashMap<String, String>();
		StringBuffer userOut = new StringBuffer();
		StringBuffer orgAssOut = new StringBuffer();
		try {
			if ( csvRecords != null && csvRecords.size() > 0) {
				for ( CSVRecord record : csvRecords ) {
					String name = record.get(5);
					if (!name.equals("User") && !users.containsKey(name) ) {
						String uid = UUID.randomUUID().toString();
						int last = name.lastIndexOf(StringUtils.SPACE);
						users.put(name, uid);
						userOut.append(name.substring(last, name.length())).append(StringUtils.COMMA);;
						userOut.append(name.substring(0, last)).append(StringUtils.COMMA);;
						userOut.append("M").append(StringUtils.COMMA);
						userOut.append(uid).append(StringUtils.COMMA);
						userOut.append(",,EMPLOYEE");
						userOut.append(IOUtils.LINE_SEPARATOR);
						
						orgAssOut.append(uid).append(StringUtils.COMMA).append("FIN").append(IOUtils.LINE_SEPARATOR);
						
					}
				}
				FileWriter w = new FileWriter(new File("/tmp/IDENTITY.csv"));
				w.write(userOut.toString());
				w.flush();
				
				FileWriter assout = new FileWriter(new File("/tmp/ORGANISATIONASSIGNMENT.csv"));
				assout.write(orgAssOut.toString());
				assout.flush();
			}
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}
		
		return users;
		
	}
	
	
	
	
	private void generateContextFiles( Map<String,RoleWrapper> roles) throws Exception{
		//value0,description0,,,fr
		//PP_FILE,Property provider with values from a file,be.personify.util.properties.provider.impl.FilePropertyProvider,"{'file' : '/tmp/file_contextvalues.csv'}"
		//ADMIN_ENT,roles,the roles subject to the assignment,true,true,1,1000,null,PP_ROLE
		int ctxCount = 1;
		StringBuffer ppOut = new StringBuffer();
		StringBuffer rolePropertyOut = new StringBuffer();
		
		for ( String key :roles.keySet()) {
			StringBuffer ctxOut = new StringBuffer();
			List<String> contexts = roles.get(key).getContexts();
			Collections.sort(contexts);
			for ( String context : contexts ) {
				ctxOut.append("CTX00" + ctxCount).append(StringUtils.COMMA);
				ctxOut.append(context).append(StringUtils.COMMA);
				ctxOut.append(",,en").append(IOUtils.LINE_SEPARATOR);
				ctxCount++;
			}
			
			
			String fileName = "/tmp/contexts/" + key + ".csv";
			FileWriter w = new FileWriter(new File(fileName));
			w.write(ctxOut.toString());
			w.flush();
			
			String proppKey = "PP_FILE_" + key;
			
			ppOut.append(proppKey).append(",Property provider with values from a file for role " + key + ",be.personify.util.properties.provider.impl.FilePropertyProvider,\"{'file' : '" + fileName + "'}\"").append(IOUtils.LINE_SEPARATOR);
			rolePropertyOut.append(key).append(",context,the context subject to the assignment,true,true,1,1000,null," + proppKey).append(IOUtils.LINE_SEPARATOR);
			//roleCount++;
		}
		
		
		FileWriter w = new FileWriter(new File("/tmp/PROPERTYPROVIDER.csv"));
		w.write(ppOut.toString());
		w.flush();
		
		w = new FileWriter(new File("/tmp/ROLEPROPERTY.csv"));
		w.write(rolePropertyOut.toString());
		w.flush();
		
		
	}
	
	
	
	
	private class RoleWrapper {
		
		private String name;
		private String code;
		
		private List<String> contexts;
		
		
		public RoleWrapper( String name, String code) {
			this.name = name;
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public List<String> getContexts() {
			return contexts;
		}

		public void setContexts(List<String> contexts) {
			this.contexts = contexts;
		}
		
		
		
	}
	

}
