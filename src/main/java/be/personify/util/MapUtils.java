package be.personify.util;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MapUtils {
	
	private static final Logger logger = LogManager.getLogger(MapUtils.class);
	
	public static void dump( Map<String,String> map ) {
		if ( map != null ) {
			for ( String key : map.keySet()) {
				logger.debug("key {} value {}", key, map.get(key)) ;
			}
		}
		else {
			logger.info("can not dump empty map");
		}
	} 
	
	
	
	public static void dumpObjectMap( Map<String,Object> map ) {
		if ( map != null ) {
			for ( String key : map.keySet()) {
				logger.debug("key {} value {}", key, map.get(key)) ;
			}
		}
		else {
			logger.info("can not dump empty map");
		}
	} 
	
	
	public static Object getKeyByValue(Map<?,?> map, Object value) {
		for ( Object o : map.keySet()) {
			logger.info("getKeyByValue {} {}", map.get(o), value) ;
			if ( map.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}

}
