package be.personify.util.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {

	private static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=utf-8";
	
	public static void sendMail(String from, String to, String subject, String content) {
		
		Session session = Session.getInstance(System.getProperties());//
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(content, TEXT_HTML_CHARSET_UTF_8);
			Transport.send(message);
		} 
		catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	public static void main(String[] args) {
		
		Properties props = System.getProperties();
	    props.put("mail.smtp.starttls.enable", "true"); // added this line
	    props.put("mail.smtp.host", "smtp.office365.com");
	    props.put("mail.smtp.user", "ciam.forgerock@glpg.com");
	    props.put("mail.smtp.password", "RTZMhsXxVDlXLgW38vUFMaMb2");
	    props.put("mail.smtp.port", "587");
	    
	    sendMail("noreply@glpg.com", "wouter.vanderbeken@idetnit.eu", "test", "test");
		
	}



}
