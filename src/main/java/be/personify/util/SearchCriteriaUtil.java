package be.personify.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import be.personify.util.exception.InvalidFilterException;

/**
 * 
 * Utility class for searchcriteria
 *
 */
public class SearchCriteriaUtil {
	
	private static final Logger logger = LogManager.getLogger(SearchCriteriaUtil.class);
	
	
	/**
	 * Composes searchcriteria from a SCIM filter string 
	 * @param filter
	 * @return
	 * @throws InvalidFilterException
	 */
	public SearchCriteria composeSearchCriteriaFromSCIMFilter(String filter) throws InvalidFilterException {
		SearchCriteria searchCriteria = new SearchCriteria();
		if (!StringUtils.isEmpty(filter)) {
			logger.info("composing searchcriteria from filter {}", filter);
			filter = filter.trim();
			filter = checkDoubleParentheses(filter);
			while( !StringUtils.isEmpty(filter)) {
				try {
					filter = addNextPart(filter,searchCriteria, null);
				}
				catch( Exception e) {
					e.printStackTrace();
					throw new InvalidFilterException(e.getMessage());
				}
			}
		}
		return searchCriteria;
	}
	
	

	private String addNextPart(String filter, SearchCriteria searchCriteria, String keyPrefix) {
		if ( StringUtils.isEmpty(filter)) {
			return filter;
		}
		filter = filter.trim();
		//logger.info("filter 1 {}", filter);
		if ( filter.startsWith("and")) {
			searchCriteria.setOperator(LogicalOperator.AND);
			filter = filter.substring(3, filter.length());
		}
		else if ( filter.startsWith("or")) {
			searchCriteria.setOperator(LogicalOperator.OR);
			filter = filter.substring(2, filter.length());
		}
		filter = filter.trim();
		//it's a group
		//logger.info("filter 2 {}", filter);
		if ( filter.startsWith("(")) {
			int closing = filter.indexOf(")");
			String subFilter = filter.substring(1, closing);
			SearchCriteria s = new SearchCriteria();
			while( !StringUtils.isEmpty(subFilter)) {
				subFilter = addNextPart(subFilter,s, null);
			}
			searchCriteria.getGroupedCriteria().add(s);
			filter = filter.substring(closing + 1, filter.length());
		}
		else {
			int spaceIndex = filter.indexOf(StringUtils.SPACE);
			//read key
			String key = filter.substring(0, spaceIndex );
			
			//complex filtering
			int complexStart = key.indexOf("[");
			if ( complexStart != -1 ) {
				String newKeyPrefix = key.substring(0,complexStart);
				int complexEnd = filter.indexOf("]");
				String complexFilter = filter.substring(complexStart +1, complexEnd);
				SearchCriteria s = new SearchCriteria();
				while( !StringUtils.isEmpty(complexFilter)) {
					complexFilter = addNextPart(complexFilter,s, newKeyPrefix);
				}
				searchCriteria.getGroupedCriteria().add(s);
				filter = filter.substring(complexEnd + 1).trim();
				return addNextPart(filter, searchCriteria, null);
			}
			else {
				filter = filter.substring(spaceIndex + 1).trim();
				//read operator
				String operator = null;
				spaceIndex = filter.indexOf(StringUtils.SPACE);
				if ( spaceIndex != -1) {
					operator = filter.substring(0, spaceIndex );
					filter = filter.substring(spaceIndex + 1).trim();
				}
				else {
					operator = filter.substring(0, filter.length() );
					filter = StringUtils.EMPTY_STRING;
				}
				SearchOperation operation = SearchOperation.operationFromString(operator.toLowerCase());
				
				//read value if necessary
				String value = null;
				if ( operation.getParts() == 3) {
					if ( filter.startsWith(StringUtils.QUOTE)){
						spaceIndex = filter.indexOf(StringUtils.QUOTE, 1);
						if ( spaceIndex != -1) {
							value = filter.substring(1, spaceIndex );
							filter = filter.substring(spaceIndex + 1).trim();
						}
						else {
							//unclosed
							throw new RuntimeException("unclosed quote");
						}
					}
					else {
						spaceIndex = filter.indexOf(StringUtils.SPACE);
						if ( spaceIndex != -1) {
							value = filter.substring(0, spaceIndex );
							filter = filter.substring(spaceIndex + 1).trim();
						}
						else {
							value = filter.substring(0, filter.length() );
							filter = StringUtils.EMPTY_STRING;
						}
						value = value.replaceAll(StringUtils.QUOTE, StringUtils.EMPTY_STRING);
					}
				}
				
				//add criteria
				if ( keyPrefix != null ) {
					key = keyPrefix + StringUtils.DOT + key;
				}
				searchCriteria.getCriteria().add(new SearchCriterium(key, value, operation));
			}
		}
		return filter;
	}


	
	

	private String checkDoubleParentheses(String filter) {
		if ( filter.startsWith(StringUtils.LEFT_PARENTHESE)) {
			boolean nextCharFound = false;
			int position = 1;
			while ( !nextCharFound ) {
				char nextChar = filter.charAt(position);
				position++;
				if ( nextChar != ' ') {
					nextCharFound = true;
					if ( nextChar == '(') {
						filter = filter.substring(position -1, filter.length() -1);
						filter.trim();
					}
				}
			}
		}
		return filter;
	}


}
