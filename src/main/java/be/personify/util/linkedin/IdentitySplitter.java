package be.personify.util.linkedin;

import be.personify.util.io.IOUtils;

public class IdentitySplitter {
	
	
	public static void main(String[] args) {
		String[] lines = IOUtils.readFileAsLines("/home/wouter/Documents/xxxxxx_employees.txt");
		StringBuffer b = new StringBuffer("");
		int count = 1;
		for ( String line : lines ) {
			if ( line.startsWith("  ")) {
				line = line.substring(2,line.length());
			}
			int i = line.indexOf(" ");
			b.append(line.substring(0, i)).append(",").append(line.substring(i + 1, line.length()));
			b.append(",M,000000000" + count + ",,,");
			b.append(IOUtils.LINE_SEPARATOR);
			count++;
		}
		System.out.println(b.toString());
	}

}
