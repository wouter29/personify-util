package be.personify.util;

import java.util.HashMap;
import java.util.Map;

public class JoinInfo {
	
	private Map<String, Join> tableJoins = new HashMap<String, Join>();
	
	
	public void addJoin( String tableName, String alias, String joinColumn ) {
		tableJoins.put(alias, new Join(tableName, alias, joinColumn));
	}
	
	
	public Map<String,Join> getJoins(){
		return tableJoins;
	}
	
	
	public class Join {
		
		private String tableName;
		private String alias;
		private String joinColumn;
			
		
		public Join(String tableName, String alias, String joinColumn ) {
			this.tableName = tableName;
			this.alias = alias;
			this.joinColumn = joinColumn;
		}


		public String getTableName() {
			return tableName;
		}


		public String getAlias() {
			return alias;
		}


		public String getJoinColumn() {
			return joinColumn;
		}
		
		
	}
	
	
}
