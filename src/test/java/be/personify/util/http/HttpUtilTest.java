package be.personify.util.http;


import org.junit.Assert;

import junit.framework.TestCase;

public class HttpUtilTest extends TestCase {
	
	
	public void testextractServerNameFromForwardedForOne() {
		String serverName = HttpUtil.extractServerNameFromForwardedFor("https://corona.cn:8080");
		Assert.assertEquals("corona.cn", serverName);
	}
	

}
