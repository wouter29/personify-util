package be.personify.util.http;


import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.util.Assert;

import be.personify.util.properties.provider.RolePropertyFilter;
import be.personify.util.properties.provider.RolePropertyProvider;
import be.personify.util.properties.provider.impl.RoleAdministratorPropertyProvider;
import junit.framework.TestCase;

public class EntitlementPropertyProviderTest extends TestCase {
	
	
	public void testGetProperties() {
		
		
		Map<String,String> configuration = new HashMap<>();
		configuration.put("clientid", "module-provisioning");
		configuration.put("secret", "Azerty1234&");
		configuration.put("authorizationUrl", "http://personify-module-authentication.personify.svc:9094/auth");
		configuration.put(RoleAdministratorPropertyProvider.CONFIGURATION_KEY_ENTITLEMENT_URL, "http://personify-backend-vault.personify.svc:9190/api/entitlement/search");
		
		
		RolePropertyProvider propertyProvider = new RoleAdministratorPropertyProvider(configuration);
		
		Assert.notNull(propertyProvider, "property provider can not be null");
		
		Map<String,String> p = propertyProvider.getProperties(new RolePropertyFilter(), Locale.ENGLISH);
		
		System.out.println(p);
		
		//Assert.notNull(p);
		
		//Assert.isTrue(p.size() > 0 );
		
	}
	

}
