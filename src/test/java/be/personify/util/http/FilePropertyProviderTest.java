package be.personify.util.http;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.util.Assert;

import be.personify.util.properties.provider.RolePropertyFilter;
import be.personify.util.properties.provider.RolePropertyProvider;
import be.personify.util.properties.provider.impl.FilePropertyProvider;
import junit.framework.TestCase;

public class FilePropertyProviderTest extends TestCase {
	
	
	public void testGetProperties() throws Exception {
		
		
		Map<String,String> configuration = new HashMap<>();
		
		writeFile(configuration,"");

		
		RolePropertyProvider propertyProvider = new FilePropertyProvider(configuration);
		
		Assert.notNull(propertyProvider,"proverty provider can not be null");
		
		Map<String,String> p = propertyProvider.getProperties(new RolePropertyFilter(), new Locale("en"));
		
		Assert.notNull(p, "map can not be null");
		
		System.out.println("p " +  p.size());
		
		Assert.isTrue(p.size() == 10 , "size has to be 10");
		
		for ( String key : p.keySet()) {
			System.err.println(key + " " + p.get(key));
		}
	}



	
	
	
	
	public void testGetPropertiesForNonExistingLocaleFallbackEnglish() throws Exception {
		
		Map<String,String> configuration = new HashMap<>();
		
		writeFile(configuration,"");

		
		RolePropertyProvider propertyProvider = new FilePropertyProvider(configuration);
		
		Assert.notNull(propertyProvider, "propertyprovider can not be null");
		
		Map<String,String> p = propertyProvider.getProperties(new RolePropertyFilter(), new Locale("nl"));
		
		Assert.notNull(p, "map can not be null");
		
		System.out.println("p " +  p.size());
		
		Assert.isTrue(p.size() == 10, "size has to be 10" );
		
		for ( String key : p.keySet()) {
			System.err.println(key + " " + p.get(key));
		}
	}
	
	
	
	
	public void testGetPropertiesForExistingLocale() throws Exception {
		
		Map<String,String> configuration = new HashMap<>();
		
		writeFile(configuration,"nl");

		
		RolePropertyProvider propertyProvider = new FilePropertyProvider(configuration);
		
		Assert.notNull(propertyProvider, "property provider can not be null");
		
		Map<String,String> p = propertyProvider.getProperties(new RolePropertyFilter(), new Locale("nl"));
		
		Assert.notNull(p, "map can not be null");
		
		System.out.println("p " +  p.size());
		
		Assert.isTrue(p.size() == 10, "size has to be 10");
		
		for ( String key : p.keySet()) {
			System.err.println(key + " " + p.get(key));
		}
	}
	
	
	
	
	public void testGetNoPropertiesForExistingLocale() throws Exception {
		
		Map<String,String> configuration = new HashMap<>();
		
		writeFile(configuration,"fr");

		
		RolePropertyProvider propertyProvider = new FilePropertyProvider(configuration);
		
		Assert.notNull(propertyProvider, "property provider can not be null");
		
		Map<String,String> p = propertyProvider.getProperties(new RolePropertyFilter(), new Locale("nl"));
		
		Assert.notNull(p, "map can not be null");
		
		System.out.println("p " +  p.size());
		
		Assert.isTrue(p.size() == 0  , "size has to be 10");
		
		
	}
	
	
	
	
	
	private void writeFile(Map<String, String> configuration, String locale) throws FileNotFoundException, IOException {
		File file = new File(System.getProperty("java.io.tmpdir") + "/test_file_pp.csv");
		FileOutputStream fos = new FileOutputStream(file);
		 
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	 
		for (int i = 0; i < 10; i++) {
			bw.write("value" + i + "," + "description" + i + ",,," + locale);
			bw.newLine();
		}
	 
		bw.close();
		
		configuration.put(FilePropertyProvider.KEY_FILE, file.getAbsolutePath());
	}
	

}
