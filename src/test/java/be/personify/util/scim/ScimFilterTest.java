package be.personify.util.scim;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import be.personify.util.LogicalOperator;
import be.personify.util.SearchCriteria;
import be.personify.util.SearchCriteriaUtil;
import be.personify.util.SearchOperation;

public class ScimFilterTest {
	
	
	
 
	private static final Logger logger = LogManager.getLogger(ScimFilterTest.class);
	
	private SearchCriteriaUtil searchCriteriaUtil = new SearchCriteriaUtil();

	
	
	@Test
	public void testOne() {
		String filter = SampleFilter.FILTER_ONE;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 1 );
			Assert.assertTrue("has to be equals",criteria.getCriteria().get(0).getSearchOperation().equals(SearchOperation.EQUALS));
			Assert.assertTrue("has to be userName",criteria.getCriteria().get(0).getKey().equals("userName"));
			Assert.assertTrue("has to be userName",criteria.getCriteria().get(0).getValue().equals("bjensen"));
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testOneBis() {
		String filter = SampleFilter.FILTER_ONE_BIS;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 1 );
			Assert.assertTrue("has to be equals",criteria.getCriteria().get(0).getSearchOperation().equals(SearchOperation.EQUALS));
			Assert.assertTrue("has to be userName",criteria.getCriteria().get(0).getKey().equals("userName"));
			Assert.assertTrue("has to be userName",criteria.getCriteria().get(0).getValue().equals("bjensen six"));
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void testTwo() {
		String filter = SampleFilter.FILTER_TWO;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 1 );
			Assert.assertTrue("has to be equals",criteria.getCriteria().get(0).getSearchOperation().equals(SearchOperation.PRESENT));
			Assert.assertTrue("has to be userName",criteria.getCriteria().get(0).getKey().equals("title"));
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testThree() {
		String filter = SampleFilter.FILTER_THREE;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.AND );
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testFour() {
		String filter = SampleFilter.FILTER_FOUR;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testFive() {
		String filter = SampleFilter.FILTER_FIVE;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.AND);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void testFiveBis() {
		String filter = SampleFilter.FILTER_FIVE_BIS;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.AND);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void testFiveTris() {
		String filter = SampleFilter.FILTER_FIVE_TRIS;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testSix() {
		String filter = SampleFilter.FILTER_SIX;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria", criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.AND);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	
	@Test
	public void testSeven() {
		String filter = SampleFilter.FILTER_SEVEN;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria should be 2 and is " + criteria.size(), criteria.size() == 2 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	
	@Test
	public void testEight() {
		String filter = SampleFilter.FILTER_EIGHT;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria should be 3 and is " + criteria.size(), criteria.size() == 3 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	
	@Test
	public void testNiner() {
		String filter = SampleFilter.FILTER_EIGHT_BIS;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria should be 3 and is " + criteria.size(), criteria.size() == 3 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	
	@Test
	public void testTen() {
		String filter = SampleFilter.FILTER_EIGHT_TRIS;
		try {
			SearchCriteria criteria = searchCriteriaUtil.composeSearchCriteriaFromSCIMFilter(filter);
			Assert.assertTrue("size of the criteria should be 3 and is " + criteria.size(), criteria.size() == 3 );
			Assert.assertTrue("logical operator of the criteria", criteria.getOperator() == LogicalOperator.OR);
			logger.info("crit {}", criteria);
		}
		catch( Exception e ) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	

}
