package be.personify.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import be.personify.util.io.EnvironmentPropertyResolver;

public class EnvironmentPropertyResolverTest {



	@Test
	public void testOne() {
		
		String testString = "${PATH}";
		String replaced = EnvironmentPropertyResolver.replace(testString);
		
		System.out.println(replaced);
		
		assertTrue(replaced.startsWith("/"));
		
	}


}
