package be.personify.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

public class DateUtilTest {
	
	
	@Test
	public void testOne() {
		String d = "2021-07-03T10:32:56.472+00:00";
		try {
			Date date = DateUtils.parse(d);
			assertTrue(date != null);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testTwo() {
		String d = "2022-07-13T00:00:00.000+00:00";
		try {
			Date date = DateUtils.parse(d);
			assertTrue(date != null);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}
	
	
}
